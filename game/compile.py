#!/usr/bin/python3

# COMPILER.PY
# Handy Typescript automater I made that automates typescript compiling, compiling to js then moving to a new directory.

import sys
import os
import json
import hashlib
import subprocess
import re
from os.path import join,exists

# Gets checksums for each file that exists from changelog. Checksums are used here to see
# if a file has been modified since last time the program was run.
def read_from_changelog(log_name=".changelog"): 
    try:
        with open(log_name, "r") as infile:
            changelog = json.loads(infile.read()) 
            return changelog
    except FileNotFoundError as e:
        raise e

# Creates new checksums for each file in the src directory, although we can make hashes for other files too.
def create_hashes(log_name=".changelog", dir_to_read="src"):
    changelog = {} 
    for root, directories, files in os.walk(dir_to_read):
        for filename in files:
            with open(join(root, filename), 'rb') as infile:
                hasher = hashlib.md5()
                data = infile.read()
                hasher.update(data)
                changelog[join(root, filename)] = hasher.hexdigest()
                
    return changelog

# writes an object to a changelog file - in this case, we use it to write our new checksums
# back to the changelog, after we have finished our checksum comparison.
def write_new_changelog(log_name=".changelog", hashes={}):
    with open(log_name, "w") as outfile:
        outfile.write(json.dumps(hashes))

# Given a path to a typescript file, this will compile the file to javascript and move
# it to core/<whatever directory it was in before>.
# BUG: IF THE DIRECTORY PATH DOES NOT EXIST IN CORE, IT WILL NOT MOVE THE FILE!
def compile_to_js(filename):
    src_js_file = re.sub('\.ts', '.js', filename)
    dst_array = src_js_file.split('/')
    dst_array[0] = "core"
    dst_js_file = '/'.join(dst_array)
    print(filename, ' --> ', dst_js_file)
    subprocess.check_call(['tsc', '--module', 'amd', filename])    
    subprocess.check_call(['mv', src_js_file, dst_js_file]) 

def flush_src_folder():
    to_remove = []
    for root, directories, files in os.walk("src"):
        for current_file in files:
            filename, file_extension = os.path.splitext(current_file)
            if (file_extension == ".js"):
                to_remove.append(join(root, current_file))
    for filename in to_remove:
        print("REMOVE -->", filename)
        subprocess.check_call(['rm', filename])

if __name__ == "__main__":
    new_hashes = create_hashes()
    changes = [] 

    try:
        old_hashes = read_from_changelog()
        for filename in old_hashes:
            if filename in new_hashes:
                old_hash_value = old_hashes[filename]
                new_hash_value = new_hashes[filename]
                if (old_hash_value != new_hash_value):
                    changes.append(filename)
            else:
                changes.append(filename)
         
    except FileNotFoundError as e:
        print("Error - Changelog not found.")
        for root, directories, files in os.walk("src"):
            for current_file in files:
                filename, file_extension = os.path.splitext(current_file)
                if (file_extension == ".ts"):
                    changes.append(join(root,current_file))

    print('--TSC Compiler--')
    print('CHANGED FILES : ', changes)
    
    for filename in changes:
        compile_to_js(filename)

    flush_src_folder()
    write_new_changelog(hashes=new_hashes)
