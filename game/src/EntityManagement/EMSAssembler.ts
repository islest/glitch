import EntityObject = require("./EMSEntity");
import BodyComponent = require("./Components/Body");;
import AngledMovementComponent = require("./Components/AngledMovement");
import StrafingMovementComponent = require("./Components/StrafingMovement");
import GraphicComponent = require("./Components/Graphics");
import ControllableComponent = require("./Components/Controllable");
import HealthComponent = require("./Components/Health");
import ShootingComponent = require("./Components/Shooter");
import BoundedComponent = require("./Components/Bounded");
import SpriteComponent = require("./Components/Sprite");
import SpriteSheet = require("../Images/SpriteSheet");
import CollisionComponent = require("./Components/Collideable");
import DecayingComponent = require("./Components/Decaying");
import AssetManager = require("../Game/AssetManager");

export function player(x: number, y: number, width: number, height: number): EntityObject {
    // Instantiate object, load assets
    var player: EntityObject = new EntityObject("PLAYER");
    var spriteImageFile: HTMLImageElement = AssetManager.getInstance().retrieveAsset('images/spaceship_spritesheet_32x32.png'); 
    var spriteSheet: SpriteSheet = new SpriteSheet(spriteImageFile, 32, 32, 3);
    
    // Load Components
    var body: BodyComponent = new BodyComponent(x, y, width, height);
    var movement: StrafingMovementComponent = new StrafingMovementComponent(0, 0, 0, 0);
    var graphic: GraphicComponent = new GraphicComponent(null);
    var controllable: ControllableComponent = new ControllableComponent();
    var sprite: SpriteComponent = new SpriteComponent(spriteSheet);

    player.addComponent(body);
    player.addComponent(movement);
    player.addComponent(graphic);
    player.addComponent(controllable);
    player.addComponent(sprite);
    player.addComponent(new ShootingComponent(3, 100));
    player.addComponent(new BoundedComponent());
    player.addComponent(new CollisionComponent(0));
    return player;
}

export function explosion(x: number, y: number, width: number, height: number): EntityObject {
    var explosion: EntityObject = new EntityObject("EXPLOSION");
    var spriteImageFile: HTMLImageElement = AssetManager.getInstance().retrieveAsset('images/explosion.png');
    var spriteSheet: SpriteSheet = new SpriteSheet(spriteImageFile, 32, 32, 7);
    spriteSheet.looping = true;
    spriteSheet.autoplay = true;

    explosion.addComponent(new BodyComponent(x, y, width, height));
    explosion.addComponent(new GraphicComponent(null));
    explosion.addComponent(new SpriteComponent(spriteSheet));
    explosion.addComponent(new HealthComponent(14));
    explosion.addComponent(new DecayingComponent());
    return explosion;
}

export function smallExplosion(x: number, y: number, width: number, height: number): EntityObject {
    var explosion: EntityObject = new EntityObject("EXPLOSION");
    var spriteImageFile: HTMLImageElement = AssetManager.getInstance().retrieveAsset('images/small_explosion_8x8.png');
    var spriteSheet: SpriteSheet = new SpriteSheet(spriteImageFile, 32, 32, 3);
    spriteSheet.looping = true;
    spriteSheet.autoplay = true;

    explosion.addComponent(new BodyComponent(x, y, width, height));
    explosion.addComponent(new GraphicComponent(null));
    explosion.addComponent(new SpriteComponent(spriteSheet));
    explosion.addComponent(new HealthComponent(3));
    explosion.addComponent(new DecayingComponent());
    return explosion;
}



export function bullet(x: number, y: number, width: number, height: number, velocity: number, angle: number): EntityObject {
    var bullet: EntityObject = new EntityObject("BULLET");
    var bulletImageFile: HTMLImageElement = AssetManager.getInstance().retrieveAsset('images/bullet_4x4.png');    
    var canvas: HTMLCanvasElement = document.createElement('canvas'); 
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(bulletImageFile, 0, 0, canvas.width, canvas.height);

    var body: BodyComponent = new BodyComponent(x, y, canvas.width, canvas.height);
    var movement: AngledMovementComponent = new AngledMovementComponent(velocity, 0, angle);
    var graphic: GraphicComponent = new GraphicComponent(canvas);

    bullet.addComponent(body);
    bullet.addComponent(movement);
    bullet.addComponent(graphic);
    bullet.addComponent(new CollisionComponent(0));
    return bullet;
}

export function turretEnemy(x: number, y: number, width: number, height: number, velocity: number, angle: number): EntityObject {
    var enemy: EntityObject = new EntityObject("ENEMY");
    var turretImageFile = AssetManager.getInstance().retrieveAsset('images/turretship_32x32.png');
    var canvas: HTMLCanvasElement = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(turretImageFile, 0, 0, canvas.width, canvas.height);

    var body: BodyComponent = new BodyComponent(x, y, width, height);
    var movement: AngledMovementComponent = new AngledMovementComponent(velocity, 0, angle);
    var graphic: GraphicComponent = new GraphicComponent(canvas);

    enemy.addComponent(body);
    enemy.addComponent(movement);
    enemy.addComponent(graphic);
    enemy.addComponent(new BoundedComponent());
    enemy.addComponent(new HealthComponent(3));
    enemy.addComponent(new CollisionComponent(0));
    return enemy;
}

export function chaserEnemy(x: number, y: number, width: number, height: number, velocity: number, angle: number): EntityObject {
    var enemy: EntityObject = new EntityObject("ENEMY");
    var turretImageFile = AssetManager.getInstance().retrieveAsset('images/flyingship_32x32.png');
    var canvas: HTMLCanvasElement = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(turretImageFile, 0, 0, canvas.width, canvas.height);

    var body: BodyComponent = new BodyComponent(x, y, width, height);
    var movement: AngledMovementComponent = new AngledMovementComponent(velocity, 0, angle);
    var graphic: GraphicComponent = new GraphicComponent(canvas);

    enemy.addComponent(body);
    enemy.addComponent(movement);
    enemy.addComponent(graphic);
    //enemy.addComponent(new BoundedComponent());
    return enemy;


}

