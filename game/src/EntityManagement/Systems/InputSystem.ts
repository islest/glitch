import System = require("./EMSSystem");
import InputHandler = require("../../InputHandler/InputHandler");
import EventManager = require("../../EventManager/EventManager");
import EntityObject = require("../EMSEntity");

class InputSystem extends System {
    public name: string;
    public inputHandler: InputHandler;

    constructor() {
        super();
        this.name = "input";
    }

    protected processItem(item: EntityObject): void {
        if (!item.components.hasOwnProperty('body')) return;
        if (!item.components.hasOwnProperty('movement')) return;
        if (!item.components.hasOwnProperty('controllable')) return;
        if (item.components.movement.moveType != "strafing") return;

        var velX: number = 0;
        var velY: number = 0;

        if (this.inputHandler.has('w')) { velY -= 6; }
        if (this.inputHandler.has('s')) { velY += 6; }
        if (this.inputHandler.has('a')) { velX -= 6; }
        if (this.inputHandler.has('d')) { velX += 6; }

        item.components.movement.velocity.x = velX;
        item.components.movement.velocity.y = velY;

        // Dis bad.
        if (item.components.hasOwnProperty('sprite')) {
          
            if (velY > 0) {
                item.components.sprite.spriteSheet.setIndex(0);
            } else if (velY < 0) {
                item.components.sprite.spriteSheet.setIndex(2);
            } else {
                item.components.sprite.spriteSheet.setIndex(1);
            }

        }

        if (this.inputHandler.has('RTN')) {
            this.broadcast(EventManager.Event.GAME_BULLETFIRED, item, null);
        }

       
      
    }
}

export = InputSystem;
