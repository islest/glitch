import System = require("./EMSSystem");
import Spritesheet = require("../../Images/SpriteSheet");
import EntityObject = require("../EMSEntity");

class AnimationSystem extends System {
    public name: string;

    constructor() {
        super();
        this.name = "animation"
    }

    protected processItem(item: EntityObject) {
        if (!item.components.hasOwnProperty('graphics')) return;
        if (!item.components.hasOwnProperty('sprite')) return;
        if (!item.components.hasOwnProperty('body')) return;

        item.components.graphics.canvas = item.components.sprite.spriteSheet.getCurrentFrame();
        
        if (item.components.sprite.spriteSheet.autoplay) {
            item.components.sprite.spriteSheet.next();
        }
    }
}

export = AnimationSystem;
