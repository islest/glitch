import EntityObject = require("../EMSEntity");
import EventManager = require("../../EventManager/EventManager");

class EMSSystem extends EventManager.Subject {
    public name: string;
    
    constructor() { 
        super();
        this.name = "";
    }

    public run(entities: any): void {
        for (var key in entities) {
            if (!entities.hasOwnProperty(key)) continue;
            this.processItem(entities[key]);
        }
    }
   
    protected processItem(item: EntityObject): void {}
}

export = EMSSystem;
