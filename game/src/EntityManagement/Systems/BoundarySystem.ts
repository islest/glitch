import System = require("./EMSSystem");
import EventManager = require("../../EventManager/EventManager");
import EntityObject = require("../EMSEntity");
import SystemData = require("../../SystemData/SystemData");

class BoundarySystem extends System { 
    public name: string;
    
    constructor() { 
        super();
        this.name = "boundary";
    }

    protected processItem(item: EntityObject) {
        if (!item.components.hasOwnProperty('body')) return;
        if (!item.components.hasOwnProperty('bounded')) {

            var entityOutOfBounds: boolean = false;

            entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.x > SystemData.SCREEN_WIDTH * 2);
            entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.x < -SystemData.SCREEN_WIDTH);
            entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.y > SystemData.SCREEN_HEIGHT * 2);
            entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.y < -SystemData.SCREEN_HEIGHT);
        
            if (entityOutOfBounds) { 
                this.broadcast(EventManager.Event.GAME_ENTITYDEAD, item, null);
            }

        } else {
            
            var x = item.components.body.pos.x;
            var y = item.components.body.pos.y;
            var width = item.components.body.width;
            var height = item.components.body.height;

            if (x + width > SystemData.SCREEN_WIDTH) { item.components.body.pos.x = SystemData.SCREEN_WIDTH - width; }
            if (x < 0) { item.components.body.pos.x = 0; }
            if (y + height > SystemData.SCREEN_HEIGHT) { item.components.body.pos.y = SystemData.SCREEN_HEIGHT - height; }
            if (y < 0 ) { item.components.body.pos.y = 0; }

        }
    }
}

export = BoundarySystem;
