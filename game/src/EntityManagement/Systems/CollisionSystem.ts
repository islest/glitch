import EntityObject = require("../EMSEntity");
import System = require("./EMSSystem");
import SpacialPartition = require("../../DataStructures/SpacialPartition");
import SystemData = require("../../SystemData/SystemData");
import EventManager = require("../../EventManager/EventManager");

class CollisionSystem extends System {
    private partition: SpacialPartition;

    constructor() {
        super();
        this.name = "collision";
        this.partition = new SpacialPartition(8, SystemData.SCREEN_WIDTH, SystemData.SCREEN_HEIGHT);
    }

    public run(entities: any): void {
        this.partition.reset();
        
        // populate this.partition so it has entities in it all divided nicely.
        for (var key in entities) {
            if (!entities.hasOwnProperty(key)) continue; 
            var current = entities[key];
            if (current.components.hasOwnProperty('body')) {
                this.partition.addItem(current.id, 
                                    current.components.body.pos.x, current.components.body.pos.y,
                                    current.components.body.width, current.components.body.height);
            
            } 
        }
    
        for (var key in entities) {
            if (!entities.hasOwnProperty(key)) continue; 
            var current = entities[key];
            if (current.components.hasOwnProperty('body')) {
                
                var neighbours = this.partition.getPossibleCollisions(current.id, 
                                    current.components.body.pos.x, current.components.body.pos.y, 
                                    current.components.body.width, current.components.body.height);

                var that = this;
                neighbours.forEach(function(entityID) {
                    if ((!entities.hasOwnProperty(entityID)) || (current == entities[entityID]))  return;
                    var currentNeighbour = entities[entityID]; 
                    var a = current.components.body;
                    var b = currentNeighbour.components.body;

                    var colliding = !(((a.pos.y + a.height) < b.pos.y) ||
                                    (a.pos.y > (b.pos.y + b.height)) ||
                                    ((a.pos.x + a.width) < b.pos.x) ||
                                    (a.pos.x > (b.pos.x + b.width)));

                    if (colliding) {

                        // If both are collideable we can signal for collision but only
                        // if cooldown has happened.
                        if ((current.components.hasOwnProperty('collideable')) &&
                            (currentNeighbour.components.hasOwnProperty('collideable'))) {  
                            if (current.components.collideable.timeUntilRefresh == 0) {  
                                that.broadcast(EventManager.Event.GAME_ENTITYCOLLISION, current, currentNeighbour);                           
                            }
                        }

                    }
                });
            }
        }
    }
}

export = CollisionSystem;
