import System = require("./EMSSystem");
import EntityObject = require("../EMSEntity");
import EventManager = require("../../EventManager/EventManager");

class DeathSystem extends System {
    public name: string;

    constructor() {
        super();
        this.name = "death";
    }

    protected processItem(item: EntityObject): void {
        if (item.components.hasOwnProperty('health')) {
            if (item.components.health.health == 0) {
                this.broadcast(EventManager.Event.GAME_ENTITYDEAD, item, null);
            }
        }
    }
}

export = DeathSystem;
