import System = require("./EMSSystem");
import EntityObject = require("../EMSEntity");

class RenderSystem extends System {
    public name: string;
    public canvas: HTMLCanvasElement;

    constructor() {
        super();
        this.name = "render";
    }

    protected processItem(item: EntityObject): void {
        if (!item.components.hasOwnProperty('body')) return; 
        if (!item.components.hasOwnProperty('graphics')) return;

        var context = this.canvas.getContext('2d');

        var buffer = document.createElement('canvas');
      
        buffer.width = item.components.graphics.canvas.width * 2;
        buffer.height = item.components.graphics.canvas.height * 2;
       
        var bufferContext = buffer.getContext('2d');

        bufferContext.translate(buffer.width/2, buffer.height/2);

        if (item.components.hasOwnProperty('movement')) {
            if (item.components.movement.moveType == "angled") {
                bufferContext.rotate(item.components.movement.angle * Math.PI/180);                
            }
        }

        bufferContext.drawImage(item.components.graphics.canvas, -buffer.width/4, -buffer.height/4);
        var x = item.components.body.pos.x - item.components.body.width/2;
        var y = item.components.body.pos.y - item.components.body.height/2;

        context.drawImage(buffer, x, y);
    }

    public run(entities: any): void {
        if (this.canvas === undefined) return; 
        var entityList = [];
        for (var key in entities) {
            if (!entities.hasOwnProperty(key)) continue;
            entityList.push(entities[key]);
        }
   
        for (var i in entityList.reverse()) {        
            this.processItem(entityList[i]);
        } 
    }
}

export = RenderSystem;
