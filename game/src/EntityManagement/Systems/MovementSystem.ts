import System = require("./EMSSystem");
import EntityObject = require("../EMSEntity");

class MovementSystem extends System {
    public name: string;
    
    constructor() {
        super();
        this.name = "movement"
    }

    protected processItem(item: EntityObject): void {
        if (!item.components.hasOwnProperty('body')) return;
        if (!item.components.hasOwnProperty('movement')) return;

        if (item.components.movement.moveType == "angled") {        
        
            item.components.body.pos.x += item.components.movement.velocity * Math.cos(Math.PI / 180 * item.components.movement.angle);
            item.components.body.pos.y += item.components.movement.velocity * Math.sin(Math.PI / 180 * item.components.movement.angle);
        
        } else if (item.components.movement.moveType == "strafing") {
            
            item.components.body.pos.x += item.components.movement.velocity.x;
            item.components.body.pos.y += item.components.movement.velocity.y;
        
        }
    }
}

export = MovementSystem;
