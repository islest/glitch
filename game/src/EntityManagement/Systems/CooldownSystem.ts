import System = require("./EMSSystem");
import EntityObject = require("../EMSEntity");

class CooldownSystem extends System {
    public name: string;

    constructor() {
        super();
        this.name = "cooldown";
    }

    protected processItem(item: EntityObject): void {
        
        // cooldown that occurs after a collision 
        if (item.components.hasOwnProperty('collideable')) {
            if (item.components.collideable.timeUntilRefresh > 0) {
                item.components.collideable.timeUntilRefresh = item.components.collideable.timeUntilRefresh - 1;
            }
        }
        
        if (item.components.hasOwnProperty('shooting')) {
            if (item.components.shooting.cooldown > 0) {
                item.components.shooting.cooldown = item.components.shooting.cooldown - 1;
            }
        }

        if (item.components.hasOwnProperty('health') && item.components.hasOwnProperty('decaying')) {
            if (item.components.health.health > 0) {
                item.components.health.health = item.components.health.health - 1;
            }

        }
    }
}

export = CooldownSystem;
