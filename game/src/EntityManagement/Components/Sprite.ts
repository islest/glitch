import Component = require("./EMSComponent");
import Sprite = require("../../Images/SpriteSheet");

class SpriteComponent implements Component {
    public name: string;
    public spriteSheet: Sprite;

    constructor(sprite: Sprite) {
        this.name = "sprite";
        this.spriteSheet = sprite;
    }
}

export = SpriteComponent;
