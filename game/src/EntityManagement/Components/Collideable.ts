import Component = require("./EMSComponent");

class CollideableComponent implements Component {
    public name: string; 
    public timeUntilRefresh: number;

    constructor(cooldownTime: number) {
        this.name = "collideable";
        this.timeUntilRefresh = 0;
    }
}

export = CollideableComponent;
