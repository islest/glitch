import Component = require("./EMSComponent");

class ControllableComponent implements Component {
    public name: string;
   
    constructor() {
        this.name = "controllable";
    }
}

export = ControllableComponent;
