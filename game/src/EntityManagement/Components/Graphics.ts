import Component = require("./EMSComponent");

class GraphicsComponent implements Component {
    public name: string;
    public canvas: HTMLCanvasElement;

    constructor(canvas: HTMLCanvasElement) {
        this.name = "graphics";
        this.canvas = canvas || undefined;
    }
}

export = GraphicsComponent;
