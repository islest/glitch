import Component = require("./EMSComponent")

class DecayingComponent implements Component {
    public name: string;

    constructor() {
        this.name = "decaying";
    }
}

export = DecayingComponent;
