import Component = require("./EMSComponent");

class BoundedComponent implements Component {
    public name: string;

    constructor() {
        this.name = "bounded";
    }

}

export = BoundedComponent;
