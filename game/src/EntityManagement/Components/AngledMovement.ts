import Component = require("./EMSComponent");

class AngledMovementComponent implements Component {
    public name: string;
    public moveType: string;
    public velocity: number;
    public acceleration: number;
    public angle: number;

    constructor(velocity: number, acceleration: number, angle: number) { 
        this.name = "movement";
        this.moveType = "angled";
        this.velocity = velocity || 0;
        this.acceleration = acceleration || 0;
        this.angle = angle || 0;
    }  
}

export = AngledMovementComponent;
