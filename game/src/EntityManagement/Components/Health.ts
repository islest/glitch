import Component = require("./EMSComponent");

class HealthComponent implements Component {
    public name: string;
    public health: number;
    
    constructor(health: number) {
        this.name = "health";
        this.health = health || 0;
    }
}

export = HealthComponent;
