import Component = require("./EMSComponent");
import Point = require("../../DataStructures/Point");

class BodyComponent implements Component {
    public name: string;
    public pos: Point
    public width: number;
    public height: number;

    constructor(x: number, y: number, width: number, height: number) {
        this.name = "body";
        this.pos = new Point(x || 0, y || 0);
        this.width = width || 0;
        this.height = height || 0;
    }
}

export = BodyComponent;
