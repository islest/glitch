import Component = require("./EMSComponent");
import Point = require("../../DataStructures/Point");

class StrafingMovementComponent implements Component {
    public name: string;
    public moveType: string;
    public velocity: Point;
    public acceleration: Point;

    constructor(velocityX: number, velocityY: number, accelerationX: number, accelerationY: number) {
        this.name = "movement";
        this.moveType = "strafing"
		this.velocity = new Point(velocityX || 0, velocityY || 0);
		this.acceleration = new Point(accelerationX || 0, accelerationY || 0);
    }
}

export = StrafingMovementComponent;
