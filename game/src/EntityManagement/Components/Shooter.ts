import Component = require("./EMSComponent");

class ShootingComponent implements Component {
    public name: string;
    public fireRate: number;
    public cooldown: number;
    public ammo: number;

    constructor(fireRate: number, ammo: number) {
        this.name = "shooting";
        this.fireRate = fireRate;
        this.ammo = ammo;
        this.cooldown = 0;
    }
}

export = ShootingComponent;
