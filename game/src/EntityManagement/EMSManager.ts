import EventManager = require("../EventManager/EventManager");
import InputHandler = require("../InputHandler/InputHandler");

import EntityObject = require("./EMSEntity");
import EntityCreator = require("./EMSAssembler");
import System = require("./Systems/EMSSystem");
import RenderingSystem = require("./Systems/RenderingSystem");
import MovementSystem = require("./Systems/MovementSystem");
import InputSystem = require("./Systems/InputSystem");
import BoundarySystem = require("./Systems/BoundarySystem");
import AnimationSystem = require("./Systems/AnimationSystem");
import CollisionSystem = require("./Systems/CollisionSystem");
import CooldownSystem = require("./Systems/CooldownSystem");
import DeathSystem = require("./Systems/DeathSystem");

import Updateable = require("../GameStateManager/Updateable");
import Renderable = require("../GameStateManager/Renderable");
import Controllable = require("../GameStateManager/Controllable");


class EMSManager extends EventManager.Subject implements EventManager.Observer,Updateable,Renderable,Controllable {
    private entities: any;
    private systems: any;
    private garbageQueue: string[];
    
    constructor() {
        super(); 
        this.entities = {};
        this.systems = {};
        this.addSystem(new MovementSystem());
        this.addSystem(new BoundarySystem());
        this.addSystem(new InputSystem());
        this.addSystem(new RenderingSystem());
        this.addSystem(new AnimationSystem());
        this.addSystem(new CollisionSystem());
        this.addSystem(new CooldownSystem()); 
        this.addSystem(new DeathSystem());
        this.addEntity(EntityCreator.player(100, 150, 32, 32));
        this.addEntity(EntityCreator.turretEnemy(120, 160, 32, 32, 2, 5));
        this.addEntity(EntityCreator.chaserEnemy(100, 100, 32, 32, 3, 45));
        this.addEntity(EntityCreator.turretEnemy(160, 200, 32, 32, 1, 270));
        this.addEntity(EntityCreator.turretEnemy(300, 300, 32, 32, 0, 0));

        this.garbageQueue = []; 
    }

    public addEntity(entity: EntityObject): void {
        this.entities[entity.id] = entity;
    }

    public addSystem(system: System): void {
        this.systems[system.name] = system;
        system.register(this);
    }
 
    public notify(e: EventManager.Event, source: any, target: any): void { 
        if (e == EventManager.Event.GAME_BULLETFIRED) {
            if (source.components.hasOwnProperty('shooting')) {
                if (source.components.shooting.cooldown == 0) {
                    source.components.shooting.cooldown = source.components.shooting.fireRate;
                    var x = source.components.body.pos.x + source.components.body.width/2;
                    var y = source.components.body.pos.y + source.components.body.height/2;
                    var angle = 0; 
                    this.addEntity(EntityCreator.bullet(x, y, 8, 8, 5, angle));
                }
            }
        } else if (e == EventManager.Event.GAME_ENTITYDEAD) {
            this.garbageQueue.push(source.id);   
            if (source.entityType != "EXPLOSION") {
                this.addEntity(EntityCreator.explosion(source.components.body.pos.x, source.components.body.pos.y, 32, 32));
            }
        } else if (e == EventManager.Event.GAME_ENTITYCOLLISION) {
            if (source.entityType == "PLAYER") return;
            if (target.entityType == "PLAYER") return
            if ((source.entityType == "BULLET") && (target.entityType = "ENEMY")) {
                
                console.log("COLLISION");

                if (source.components.hasOwnProperty("health")) {
                    console.log("DECREASING HEALTH");
                    source.components.health.health = source.components.health.health - 1;
                } else {
                    this.addEntity(EntityCreator.smallExplosion(source.components.body.pos.x, source.components.body.pos.y, 32, 32));
                    delete this.entities[source.id]; 
                }

                if (target.components.hasOwnProperty("health")) {
                    target.components.health.health = target.components.health.health - 1;
               } else {
                    delete this.entities[target.id];
                }
            }
        }
    }

    public handleInput(input: InputHandler): void {
        this.systems.input.inputHandler = input;
        this.systems.input.run(this.entities);
    }
    
    public update(): void {
        for (var i = 0; i < this.garbageQueue.length; i++) { 
            delete this.entities[this.garbageQueue[i]];
        } 
        this.systems.movement.run(this.entities); 
        this.systems.boundary.run(this.entities);
        this.systems.cooldown.run(this.entities);
        this.systems.collision.run(this.entities);
        this.systems.death.run(this.entities);      
    }
    
    public render(canvas: HTMLCanvasElement): void { 



        this.systems.animation.run(this.entities);
        this.systems.render.canvas = canvas;
        this.systems.render.run(this.entities);
    }
}

export = EMSManager;
