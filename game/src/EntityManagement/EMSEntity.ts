import Component = require("./Components/EMSComponent");

class EntityObject {
    static numEntities: number = 0;
    public id: string;
    public entityType: string;
    public components: any;

    constructor(entityType: string) {
        var date = new Date();
        this.id = date.getMinutes().toString();
        this.id += date.getSeconds().toString();
        this.id += date.getMilliseconds().toString();
        this.id += (Math.random() * 100000000 | 0).toString();
        this.id += EntityObject.numEntities.toString();
       
        this.entityType = entityType;
        this.components = {};
        EntityObject.numEntities++;
    }

    public addComponent(component: Component): void {
        this.components[component.name] = component;
    }

    public removeComponent(componentName: string): void {
        if (componentName in this.components) {
			this.components[componentName] = null;
		}
    }
}

export = EntityObject;
