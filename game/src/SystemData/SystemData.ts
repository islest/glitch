class SystemData {
    public static SCREEN_WIDTH: number = 640; 
    public static SCREEN_HEIGHT: number = 480;
    public static CENTER_X: number = SystemData.SCREEN_WIDTH/2;
    public static CENTER_Y: number = SystemData.SCREEN_HEIGHT/2;
	public static RESOURCE_PATH: string = "/resources/";
}

export = SystemData;
