import Updateable = require("../GameStateManager/Updateable");
import Renderable = require("../GameStateManager/Renderable");

class StaticImage implements Updateable, Renderable {
    private image: HTMLImageElement;
    private x: number;
    private y: number;
    private speed: number;

    private scrolling: boolean;
    private pattern: boolean;

    constructor(imageFile: HTMLImageElement, x: number, y: number) {
        this.image = imageFile;  
        this.x = x || 0;
        this.y = y || 0;
        this.speed = 0;
        this.scrolling = false;
        this.pattern = false;
    }

    public setSpeed(speed: number) {
        this.speed = speed;
    }

    public setScrolling(scrolling: boolean) {
        this.scrolling = scrolling;
    }

    public setPattern(pattern: boolean) {
        this.pattern = pattern;
    }

    public update() { 
        if (this.scrolling) { 
            this.x -= this.speed;
            if (this.x <= 0) {
                this.x += this.image.width;
            }   
        }
    }

    public render(canvas: HTMLCanvasElement) {
        var context: any = canvas.getContext('2d'); 
        if (this.pattern) {
            context.drawImage(this.image, this.x - this.image.width, this.y - this.image.height);
            context.drawImage(this.image, this.x - this.image.width, this.y);
            context.drawImage(this.image, this.x - this.image.width, this.y + this.image.height);
            context.drawImage(this.image, this.x, this.y - this.image.height);
            context.drawImage(this.image, this.x, this.y);
            context.drawImage(this.image, this.x, this.y + this.image.height);
            context.drawImage(this.image, this.x + this.image.width, this.y - this.image.height);
            context.drawImage(this.image, this.x + this.image.width, this.y);
            context.drawImage(this.image, this.x + this.image.width, this.y + this.image.height);  
        } else {
            context.drawImage(this.image, this.x, this.y);
        }
    }
}

export = StaticImage;
