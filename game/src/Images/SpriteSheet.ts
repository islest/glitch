// THIS CLASS NEEDS RECODING!

class Sprite {
    private source: HTMLImageElement;
    private frames: HTMLCanvasElement[];
    private index: number;
    public looping: boolean;
    public autoplay: boolean; 

    constructor(image: HTMLImageElement, frameWidth: number, frameHeight: number, numFrames: number) {
        this.looping = false;
        this.autoplay = false;     
        this.frames = [];
        this.index = 0;

        this.source = image;
       
        for (var i = 0; i < numFrames; ++i) {
            var canvas = document.createElement('canvas');
            canvas.width = frameWidth;
            canvas.height = frameHeight;
            var ctx = canvas.getContext('2d');
            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(0, 0, frameHeight, frameWidth);
            this.frames.push(canvas);
        }
    
        for (var i = 0; i < numFrames; ++i) {
            var canvas = this.frames[i];
            var context = canvas.getContext('2d');
            context.clearRect(0, 0, frameWidth, frameHeight);
            context.drawImage(this.source, i*frameWidth, 0, frameWidth, frameHeight, 0, 0, frameWidth, frameHeight);        
        }    
    }

    public next(): void {
        var increment = 0;
        if (this.index == this.frames.length-1) {
            if (this.looping) {
                this.index = 0; 
            }
        } else {
            this.index++; 
        } 
    }

    public previous(): void {
        var increment = 0;
        if (this.index == 0) {
            if (this.looping) {
                increment = this.frames.length-1;
            }
        } else {
            increment = -1;
        }
        this.index += increment;
    }

    public reset(): void {
        this.index = 0;
    }

    public getIndex(): number {
        return this.index;
    }

    public setIndex(i: number): void {
        this.index = i;
    }

    public getCurrentFrame(): HTMLCanvasElement {
        return this.frames[this.index];
    }

    public getFrameLength(): number {
        return this.frames.length;
    }
}

export = Sprite;
