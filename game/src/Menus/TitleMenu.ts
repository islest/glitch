import InputHandler = require("../InputHandler/InputHandler");
import Menu = require("./BaseMenu");
import EventManager = require("../EventManager/EventManager");

class TitleMenu extends Menu.BaseMenu {  
    constructor() {
        super();
        var play: Menu.MenuItem = new Menu.MenuItem("Play");
        var about: Menu.MenuItem = new Menu.MenuItem("About");
        var exit: Menu.MenuItem = new Menu.MenuItem("Exit");
        this.items = [play, about, exit];
    }
    
    public activate(): void {
        if (this.currentItem == 0) {
            console.log("BROADCASTING STARTGAME EVENT");
            this.broadcast(EventManager.Event.SYSTEM_STARTGAME, null, null);
        }
    }

	render(canvas: HTMLCanvasElement): void {
        var context: any = canvas.getContext('2d');
        context.font = "20px Arial";
        
        for (var i = 0; i < this.items.length; i++) {
            context.fillStyle = "#555555"; 
           
            if (i == this.currentItem) {
                context.fillStyle = "#0000FF";
                context.fillRect(this.pos.x - 20, this.pos.y + i*100 - 10, 10, 10);
                context.fillStyle = "#FFFFFF";
            } 
            context.fillText(this.items[i].description, this.pos.x, this.pos.y + i*100); 
        }
    }
}

export = TitleMenu;
