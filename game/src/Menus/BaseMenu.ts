import InputHandler = require('../InputHandler/InputHandler');
import EventManager = require('../EventManager/EventManager');
import Renderable  = require('../GameStateManager/Renderable');
import Updateable =  require('../GameStateManager/Updateable');
import Controllable = require('../GameStateManager/Controllable');
import Point = require("../DataStructures/Point");

export class MenuItem {
    constructor(description: string) {
        this.description = description;
    }
    public description: string;
}

export class BaseMenu extends EventManager.Subject implements Renderable, Updateable, Controllable { 
    protected items: MenuItem[];
    protected currentItem: number;
	protected pos: Point;

    
    constructor() {
        super();
        this.items = [];
        this.currentItem = 0;
        this.pos = new Point(0, 0);
    }

    setPosition(x: number, y: number): void {
        this.pos = new Point(x || 0, y || 0);
    }

    moveUp(): void {
        this.currentItem -= 1;
        if (this.currentItem < 0) {
            this.currentItem = 0;
        } 
    }

    moveDown(): void {
        this.currentItem += 1;
        if (this.items.length < 1) {
            this.currentItem = 0;
        } else if (this.currentItem > this.items.length-1) {
            this.currentItem = this.items.length-1;
        }
    }

    moveRight(): void {
        this.currentItem -= 1;
        if (this.currentItem < 0) {
            this.currentItem = 0;
        }
    }

    moveLeft(): void {
        this.currentItem += 1;
        if (this.items.length < 1) {
            this.currentItem = 0;
        } else if (this.currentItem > this.items.length-1) {
            this.currentItem = this.items.length-1;
        }
    }

    activate(): void {}
    
    handleInput(input: InputHandler): void {
         
        if (input.has('s')) {
            this.moveDown();
            console.log('Moving selection upwards!');
        } else if (input.has('w')) {
            this.moveUp();
            console.log('Moving selection downwards!');
        }
        if (input.has('RTN')) {
            this.activate();
          
        }
        input.flush();
    }

    update(): void {}
    render(canvas:HTMLCanvasElement): void {}
}


