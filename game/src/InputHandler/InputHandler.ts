// Well, this class needs fixing.

class InputHandler {
    private map: any;
    
    constructor() {
        this.map = {};
    }
 
    private translateKeyCode(key: any) {
        var code: number = key.keyCode;
        
        // If key is spacebar
        if (code == 32) return ' ';

        // If key is enter
        if (code == 13) return 'RTN';
        
        // If key is alphanumeric
        if (code >= 48 && code <= 90) 
        {
            return String.fromCharCode(code).toLowerCase();
        }

        // Key is invalid
        return null;
    }

    public has(character: string): boolean {
        if (character in this.map) {
            if (this.map[character] == true) {
                return true;
            }
        }
        return false;
    }

    public print(): void {
        //console.log(this.map);
    }

    public register() {
        var inputHandler : InputHandler = this;
        window.addEventListener('keydown', function(e: any) { inputHandler.add(e); }, false);
        window.addEventListener('keyup', function(e: any) { inputHandler.remove(e); }, false);
    }

    public add(keyEvent: any) {
        var keyCode = this.translateKeyCode(keyEvent);
        if (keyCode !== null) {
            this.map[keyCode] = true;
        }
    }
    
    public remove(keyEvent: any) {
        var keyCode = this.translateKeyCode(keyEvent);
        if (keyCode !== undefined) {
            if (this.map[keyCode] !== undefined) {
                delete this.map[keyCode]
            }
        }
    }
   
    public flush() {
        this.map = {};
    }
}

export = InputHandler;
