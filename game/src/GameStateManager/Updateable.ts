interface Updateable {
    update(): void;
}

export = Updateable;
