import BaseState = require("./BaseState");
import InputHandler = require("../InputHandler/InputHandler");
import BaseMenu = require("../Menus/BaseMenu");
import TitleMenu = require("../Menus/TitleMenu");
import EventManager = require("../EventManager/EventManager");
import StaticImage = require("../Images/StaticImage");
import SystemData = require("../SystemData/SystemData");
import AssetManager = require("../Game/AssetManager");

class TitleScreen extends BaseState {
  
    constructor() {
        super();
        var menu = new TitleMenu();
        menu.register(this);
        menu.setPosition(SystemData.CENTER_X, SystemData.CENTER_Y);
        
        var logoImage = AssetManager.getInstance().retrieveAsset("images/logo.png");
        var bgImage = AssetManager.getInstance().retrieveAsset("images/background.png")
         
        var logo = new StaticImage(logoImage, 70, 30);
        var bg = new StaticImage(bgImage, 640, 0)
        bg.setScrolling(true);
        bg.setPattern(true);

        var bg2 = new StaticImage(bgImage, 640, 240);
        bg2.setScrolling(true);
        bg2.setPattern(true);
        bg2.setSpeed(2);

        this.updateables.push(bg);
        this.updateables.push(bg2);
        this.updateables.push(logo);
        this.updateables.push(menu);
        this.renderables.push(bg);
        this.renderables.push(bg2);
        this.renderables.push(logo);
        this.renderables.push(menu);
        this.controllables.push(menu);
    }

    public init(): void {
        console.log("Initialising Title Screen!");
    }
    
    public notify(e: EventManager.Event, source: any, target: any) {
        this.broadcast(e, source, target); 
    }
}

export = TitleScreen;
