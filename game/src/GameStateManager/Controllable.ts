import InputHandler = require("../InputHandler/InputHandler");

interface Controllable {
    handleInput(input: InputHandler): void;
}

export = Controllable;
