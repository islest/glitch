interface Renderable {
    render(canvas: HTMLCanvasElement): void;
}

export = Renderable;
