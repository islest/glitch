import BaseState = require("./BaseState");
import TitleScreen = require("./TitleScreen");
import MainGame = require("./MainGame");
import InputHandler = require("../InputHandler/InputHandler");
import EventManager = require("../EventManager/EventManager");

class GameStateManager extends EventManager.Subject implements EventManager.Observer {
    private gameStack: BaseState[];

    constructor() {
        super();
        this.gameStack = [];
    }

    public init(): void { 
        this.push(new TitleScreen());
    }

    public push(state: BaseState): void {
        state.register(this);
        this.gameStack.push(state);
    }

    public pop(state: BaseState): void {
        this.gameStack.pop();
    }

    public handleInput(input: InputHandler): void {
        input.print(); 
        this.gameStack[this.gameStack.length-1].handleInput(input);
    }

    public update(): void { 
        this.gameStack[this.gameStack.length-1].update();
    }

    public render(canvas: HTMLCanvasElement): void {
        this.gameStack[this.gameStack.length-1].render(canvas);
    }
 	
 	public notify(e: EventManager.Event, src: any, target: any): void {
        if (e == EventManager.Event.SYSTEM_STARTGAME) {
            console.log("CHANGING TO MAINGAME"); 
            this.push(new MainGame());  
        } 
    }
}

export = GameStateManager;
