import BaseState = require("./BaseState");
import StaticImage = require("../Images/StaticImage");
import InputHandler = require("../InputHandler/InputHandler");
import SystemData = require("../SystemData/SystemData");
import EventManager = require("../EventManager/EventManager");
import AssetManager = require("../Game/AssetManager");

class MainGame extends BaseState {   
    constructor() {
        super();

        var cloudImage: HTMLImageElement = AssetManager.getInstance().retrieveAsset('images/weird.png');
        var starImage: HTMLImageElement = AssetManager.getInstance().retrieveAsset('images/background.png');
        
        var bgZero = new StaticImage(cloudImage, 0, 0);
       	bgZero.setScrolling(false);
   
       	var bgOne = new StaticImage(starImage, SystemData.SCREEN_WIDTH, SystemData.SCREEN_HEIGHT/3);
       	bgOne.setScrolling(true);
       	bgOne.setPattern(true);
       	bgOne.setSpeed(1);
    
   		var bgTwo = new StaticImage(starImage, SystemData.SCREEN_WIDTH, 0);     
     	bgTwo.setScrolling(true);
        bgTwo.setPattern(true);
        bgTwo.setSpeed(2);

        var bgThree = new StaticImage(starImage, SystemData.SCREEN_WIDTH, SystemData.SCREEN_HEIGHT/4);
        bgThree.setScrolling(true);
        bgThree.setPattern(true);
        bgThree.setSpeed(3);
    
    	this.updateables.push(bgZero);
    	this.updateables.push(bgOne);
    	this.updateables.push(bgTwo);
    	this.updateables.push(bgThree);

    	this.renderables.push(bgZero);
    	this.renderables.push(bgOne);
    	this.renderables.push(bgTwo);
    	this.renderables.push(bgThree);
    }

    public init(): void {
        console.log("Initialising Main Game Screen!");
    }

    public notify(e: EventManager.Event, source: any, target: any) {
        this.broadcast(e, this, target);
    }

}

export = MainGame;
