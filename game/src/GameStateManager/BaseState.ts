import InputHandler = require("../InputHandler/InputHandler");
import EventManager = require("../EventManager/EventManager");
import Updateable = require("./Updateable");
import Renderable = require("./Renderable");
import Controllable = require("./Controllable");
import EMSManager = require("../EntityManagement/EMSManager");

class BaseState extends EventManager.Subject implements EventManager.Observer, Updateable, Renderable, Controllable {
    protected updateables: Updateable[];
    protected renderables: Renderable[];
    protected controllables: Controllable[];
	protected entityManager: EMSManager;

	constructor() {
        super();
        this.updateables = [];
        this.renderables = [];
        this.controllables = [];
        this.entityManager = new EMSManager();
    }
    
    init(): void {}
    
    handleInput(input: InputHandler): void {
        this.controllables.forEach(function(item) {
            item.handleInput(input);
        });
        this.entityManager.handleInput(input);
    }

    update(): void {
        this.updateables.forEach(function(item) {
            item.update();
        });
        this.entityManager.update();
    }

    render(canvas: HTMLCanvasElement): void {
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = "#000000";
        ctx.fillRect(0,0,canvas.width, canvas.height);
        this.renderables.forEach(function(item) {
            item.render(canvas);
        });
        this.entityManager.render(canvas);
    }

	notify(e : EventManager.Event, src: any, target: any) {} 
}

export = BaseState;
