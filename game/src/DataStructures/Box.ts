//Comment
class Box {
    public width: number;
    public height: number;

    constructor(width: number, height: number) {
        this.width = width || 0;
        this.height = height || 0; 
    }
}

export = Box;
