import Point = require("./Point");
import Box = require("./Box");

class AxisAlignedBoundingBox {
    public position: Point;
    public dimensions: Box;
    public id: string;

    constructor(id: string, x: number, y: number, width: number, height: number) {
        this.position = new Point(x, y);
        this.dimensions = new Box(width, height);
        this.id = id;
    } 
}

export = AxisAlignedBoundingBox;
