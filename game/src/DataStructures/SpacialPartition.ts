import Point = require("./Point");
import Box = require("./Box");
import AABB = require("./AxisAlignedBoundingBox");

class PartitionCell {
    private list: AABB[];

    constructor() {
        this.list = [];
    }

    public add(a: AABB): void {
        this.list.push(a);
    }

    public flush(): void {
        this.list = [];
    }

    public retrieveList(): AABB[] {
        return this.list;
    }
}

class SpacialPartition {
    private partitions: PartitionCell[][];
    private numberOfCells: number;
    private dimensions: Box; 

    // Constructor
    constructor(numCells: number, width: number, height: number) {
        this.numberOfCells = numCells;
        this.dimensions = new Box(width, height);
        this.partitions = [];

        for (var i = 0; i < this.numberOfCells; ++i) {
            var newList = [];
            for (var j = 0; j < this.numberOfCells; ++j) {
                newList.push(new PartitionCell());
            }
            this.partitions.push(newList);
        }
    }


    // Takes in a list of boxes and populates the partition with them. Each box has
    // an id that links it to its entity.
    public addItem(id: string, x: number, y: number, width: number, height: number): void {
        var newBox = new AABB(id, x, y, width, height);
        var itemLocation = this.getCellLocation(newBox.position.x, newBox.position.y);
        
        if (itemLocation != undefined) {
            itemLocation.add(newBox);     
        } 
    }

    // Returns an array of bounding boxes which might collide with the box provided.
    public getPossibleCollisions(id: string, x: number, y: number, width: number, height: number): any {
        var listOfCells = [];

        // Just need to push more cells here! 
        var xInterval = this.dimensions.width / this.numberOfCells;
        var yInterval = this.dimensions.height / this.numberOfCells;
       
        for (var i = 0; i * yInterval < height; ++i) {
            var stepY = i * yInterval;
            for (var j = 0; j * xInterval < width; ++j) {
                var stepX = j * xInterval;
                listOfCells.push(this.getCellLocation(x + stepX, y + stepY));
            }
        } 
       
        var uniqueCells = [];
        listOfCells.forEach(function(item) {
            if (item != undefined) {
                if (uniqueCells.indexOf(item) == -1) {
                    uniqueCells.push(item);
                }
            }
        });

        var result = [];
        uniqueCells.forEach(function(cell) {
            cell.retrieveList().forEach(function(item) {
                if (item.id != id) {
                    result.push(item.id);
                }
            });
        });

        return result;
    }

    public reset(): void {
        this.partitions.forEach(function(item, i) {
            item.forEach(function(cell, j) {
                cell.flush();
            });
        });
    }

    public print(): void {
        this.partitions.forEach(function(item, i) {
            item.forEach(function(cell, j) {
                console.log("(", i, ", ", j, ") ", cell);
                cell.retrieveList().forEach(function(entity, index) {
                    console.log(entity); 
                });
            });
        });
    }

    private getCellLocation(x: number, y: number) {
        var xInterval = (this.dimensions.width / this.numberOfCells);
        var yInterval = (this.dimensions.height / this.numberOfCells);

        var xPosition = Math.floor(x / xInterval);
        var yPosition = Math.floor(y / yInterval);

        if ((xPosition < this.numberOfCells) && (xPosition >= 0)) {
            if ((yPosition < this.numberOfCells) && (yPosition >= 0)) {
                return this.partitions[yPosition][xPosition];
            }
        }

        // It exists outside the bounds of the screen...Therefore, we want it to not collide with
        // anything, basically. So we return a new cell with an empty list.
        return null; 
    }
}

export = SpacialPartition;
