export enum Event {
    // System events
    SYSTEM_STARTGAME,
    SYSTEM_GAMEOVER,
    SYSTEM_VICTORY,
    SYSTEM_PAUSE,
    GAME_ENTITYDEAD,
    GAME_BULLETFIRED,
    GAME_ENTITYCOLLISION
}

export interface Observer {
    notify(e: Event, source: any, target: any): void;
} 

export class Subject {
    protected observers: Observer[];
    
    constructor() { 
        this.observers = [];
    }

    protected broadcast(e: Event, src: any, target: any): void {
        for (var i = 0; i < this.observers.length; ++i) {
            var current: Observer = this.observers[i];
            current.notify(e, src, target);
        }
    }

    public register(o: Observer): void {
        this.observers.push(o);
    }
}
