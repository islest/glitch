class GameLoop {
    static FPS = 60;
        
    private running: boolean;
    private step: number;
    private now: number; 
    private last: number;
    private dt: number;

    private canvas: any;
    private updateCallback: any;
    private renderCallback: any;

    constructor() {
        this.canvas = null;
        this.updateCallback = function() {};
        this.renderCallback = function() {};

        this.now = 0;
        this.dt = 0;
        this.last = Date.now();
        this.step = 1 / GameLoop.FPS;
        this.running = true;
    }
	
	public setUpdate(callback: any): void {
        this.updateCallback = callback;   
    }

    public setRender(callback: any): void {
        this.renderCallback = callback;
    }

    public start(): void {
        this.running = true;
        this.loop();
    }

    public stop(): void {
        this.running = false;
    }

    public isRunning(): boolean {
        return this.running;
    }

	private loop(): void {
        this.now = Date.now();
        if (this.running) {
            this.dt = this.dt + Math.min(1, (this.now - this.last) / 1000);
            while (this.dt > this.step) {
                this.dt -= this.step;
                this.updateCallback();
            }
 
            this.renderCallback();
            this.last = this.now; 
            window.requestAnimationFrame(this.loop.bind(this));
        }
    }
}

export = GameLoop;
