interface Asset {
	assetType: AssetType;
	path: string;
}

enum AssetType {
	IMAGE,
	SOUND
}

class AssetManager {
	public static AssetType = AssetType;
	private static instance:AssetManager = new AssetManager();
	public downloadCallback: any;
    private downloadQueue: Asset[];
	private cache: {};
    private successCount: number;
	private errorCount: number;
	private rootResourcePath: string;
    
	constructor() {
		if (AssetManager.instance) { 
			throw new Error("Instantiation Failed of AssetManager Singleton");
		}
		
		this.downloadQueue = [];
		this.successCount = 0;
		this.errorCount = 0;
		this.cache = {};
		this.rootResourcePath = "";
		AssetManager.instance = this;
	}

	// Facilitates singleton instance of AssetManager
	public static getInstance(): AssetManager {
		return this.instance;
	}

	// Assets are stored by filename, but we set the root directory, where the files
	// are located, using this function
	public setRootResourcePath(rootDirectory: string) {
		this.rootResourcePath = rootDirectory;
	}

	// Adds a file to the pathname, along with the type it was.
	public addAsset(filename: string, format: AssetType): void {
		var asset: Asset = {path: filename, assetType: format};
		this.downloadQueue.push(asset);
	}

	// Downloads everything in the download queue
	public downloadAll(): void {
		console.log(this.downloadQueue.length);
		if (this.downloadQueue.length == 0) {
            that.downloadCallback();
        }
        for (var i = 0; i < this.downloadQueue.length; ++i) {
			var asset = this.downloadQueue[i];
			var resource = undefined;

			if (asset.assetType == AssetManager.AssetType.IMAGE) {	
				resource = new Image(); 
			} else if (asset.assetType == AssetManager.AssetType.SOUND) {
				resource = new Audio();
			}
	
			var that = this;
			resource.addEventListener("load", function() {
				that.successCount += 1;
			    if (that.isFinished()) {
                    that.downloadCallback();
                }
            });

			resource.addEventListener("error", function() {
				that.errorCount += 1;
		        console.log("Error!");
                if (that.isFinished()) {
                    that.downloadCallback(); 
                }
			});
			resource.src = "";
			resource.src = this.rootResourcePath.concat(asset.path);
		    this.cache[asset.path] = resource;
        } 
        console.log(this.cache);
	}

	// Checks if all the images have finished downloading
	public isFinished(): boolean {
		return (this.downloadQueue.length == 0 || this.downloadQueue.length == (this.successCount + this.errorCount));
	}

	// Retrieves an asset by path name
	public retrieveAsset(path: string): any {
		 
        if (path in this.cache) {
			return this.cache[path];
		} else {
			return null;
		}
	}
}

export = AssetManager;
