import InputHandler = require('../InputHandler/InputHandler');
import GameLoop = require('./GameLoop');
import GameStateManager = require('../GameStateManager/GameStateManager');
import SystemData = require("../SystemData/SystemData");
import AssetManager = require("../Game/AssetManager");

class Game {
    private loop: GameLoop;   
    private input: InputHandler;
    private stateManager: GameStateManager;
    private canvas: HTMLCanvasElement

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
        this.input = new InputHandler();
        this.loop = new GameLoop();
        this.stateManager = new GameStateManager(); 
    }


	private load_resources(): void {
		var assetManager = AssetManager.getInstance();
		assetManager.setRootResourcePath(SystemData.RESOURCE_PATH);
		assetManager.addAsset("images/logo.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/background.png", AssetManager.AssetType.IMAGE);
		assetManager.addAsset("images/spaceship_spritesheet_32x32.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/turretship_32x32.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/bullet_4x4.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/weird.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/spaceinvader_32x32.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/flyingship_32x32.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/explosion.png", AssetManager.AssetType.IMAGE);
        assetManager.addAsset("images/small_explosion_8x8.png", AssetManager.AssetType.IMAGE);
        assetManager.downloadAll(); 
	}

    public init(): void {
        var that = this;
        var assetManager = AssetManager.getInstance();
        assetManager.downloadCallback = function() {
            console.log("All assets loaded, going now");
            var state = that.stateManager;
            var keyInput = that.input;
            var canvas = that.canvas;
       

            // Initialises the state manager by pushing on a title screen state.
            state.init();
            
            // Registers the input handler to the window to listen to key events.
            keyInput.register();

            var update = function(gsm: GameStateManager, input: InputHandler) {
                gsm.handleInput(input);
                gsm.update();
            };
          
            var render = function(gsm: GameStateManager, canvas: HTMLCanvasElement) {
                gsm.render(canvas);
            };

            that.loop.setUpdate(function() { update(state, keyInput); });
            that.loop.setRender(function() { render(state, canvas); });
            that.loop.start();
        }    
        this.load_resources();	
    }
}

export = Game;
