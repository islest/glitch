var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem"], function (require, exports, System) {
    var RenderSystem = (function (_super) {
        __extends(RenderSystem, _super);
        function RenderSystem() {
            _super.call(this);
            this.name = "render";
        }
        RenderSystem.prototype.processItem = function (item) {
            if (!item.components.hasOwnProperty('body'))
                return;
            if (!item.components.hasOwnProperty('graphics'))
                return;
            var context = this.canvas.getContext('2d');
            var buffer = document.createElement('canvas');
            buffer.width = item.components.graphics.canvas.width * 2;
            buffer.height = item.components.graphics.canvas.height * 2;
            var bufferContext = buffer.getContext('2d');
            bufferContext.translate(buffer.width / 2, buffer.height / 2);
            if (item.components.hasOwnProperty('movement')) {
                if (item.components.movement.moveType == "angled") {
                    bufferContext.rotate(item.components.movement.angle * Math.PI / 180);
                }
            }
            bufferContext.drawImage(item.components.graphics.canvas, -buffer.width / 4, -buffer.height / 4);
            var x = item.components.body.pos.x - item.components.body.width / 2;
            var y = item.components.body.pos.y - item.components.body.height / 2;
            context.drawImage(buffer, x, y);
        };
        RenderSystem.prototype.run = function (entities) {
            if (this.canvas === undefined)
                return;
            var entityList = [];
            for (var key in entities) {
                if (!entities.hasOwnProperty(key))
                    continue;
                entityList.push(entities[key]);
            }
            for (var i in entityList.reverse()) {
                this.processItem(entityList[i]);
            }
        };
        return RenderSystem;
    })(System);
    return RenderSystem;
});
