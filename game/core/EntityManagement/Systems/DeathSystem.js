var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem", "../../EventManager/EventManager"], function (require, exports, System, EventManager) {
    var DeathSystem = (function (_super) {
        __extends(DeathSystem, _super);
        function DeathSystem() {
            _super.call(this);
            this.name = "death";
        }
        DeathSystem.prototype.processItem = function (item) {
            if (item.components.hasOwnProperty('health')) {
                if (item.components.health.health == 0) {
                    this.broadcast(EventManager.Event.GAME_ENTITYDEAD, item, null);
                }
            }
        };
        return DeathSystem;
    })(System);
    return DeathSystem;
});
