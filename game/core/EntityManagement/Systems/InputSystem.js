var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem", "../../EventManager/EventManager"], function (require, exports, System, EventManager) {
    var InputSystem = (function (_super) {
        __extends(InputSystem, _super);
        function InputSystem() {
            _super.call(this);
            this.name = "input";
        }
        InputSystem.prototype.processItem = function (item) {
            if (!item.components.hasOwnProperty('body'))
                return;
            if (!item.components.hasOwnProperty('movement'))
                return;
            if (!item.components.hasOwnProperty('controllable'))
                return;
            if (item.components.movement.moveType != "strafing")
                return;
            var velX = 0;
            var velY = 0;
            if (this.inputHandler.has('w')) {
                velY -= 6;
            }
            if (this.inputHandler.has('s')) {
                velY += 6;
            }
            if (this.inputHandler.has('a')) {
                velX -= 6;
            }
            if (this.inputHandler.has('d')) {
                velX += 6;
            }
            item.components.movement.velocity.x = velX;
            item.components.movement.velocity.y = velY;
            // Dis bad.
            if (item.components.hasOwnProperty('sprite')) {
                if (velY > 0) {
                    item.components.sprite.spriteSheet.setIndex(0);
                }
                else if (velY < 0) {
                    item.components.sprite.spriteSheet.setIndex(2);
                }
                else {
                    item.components.sprite.spriteSheet.setIndex(1);
                }
            }
            if (this.inputHandler.has('RTN')) {
                this.broadcast(EventManager.Event.GAME_BULLETFIRED, item, null);
            }
        };
        return InputSystem;
    })(System);
    return InputSystem;
});
