var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem", "../../DataStructures/SpacialPartition", "../../SystemData/SystemData", "../../EventManager/EventManager"], function (require, exports, System, SpacialPartition, SystemData, EventManager) {
    var CollisionSystem = (function (_super) {
        __extends(CollisionSystem, _super);
        function CollisionSystem() {
            _super.call(this);
            this.name = "collision";
            this.partition = new SpacialPartition(8, SystemData.SCREEN_WIDTH, SystemData.SCREEN_HEIGHT);
        }
        CollisionSystem.prototype.run = function (entities) {
            this.partition.reset();
            // populate this.partition so it has entities in it all divided nicely.
            for (var key in entities) {
                if (!entities.hasOwnProperty(key))
                    continue;
                var current = entities[key];
                if (current.components.hasOwnProperty('body')) {
                    this.partition.addItem(current.id, current.components.body.pos.x, current.components.body.pos.y, current.components.body.width, current.components.body.height);
                }
            }
            for (var key in entities) {
                if (!entities.hasOwnProperty(key))
                    continue;
                var current = entities[key];
                if (current.components.hasOwnProperty('body')) {
                    var neighbours = this.partition.getPossibleCollisions(current.id, current.components.body.pos.x, current.components.body.pos.y, current.components.body.width, current.components.body.height);
                    var that = this;
                    neighbours.forEach(function (entityID) {
                        if ((!entities.hasOwnProperty(entityID)) || (current == entities[entityID]))
                            return;
                        var currentNeighbour = entities[entityID];
                        var a = current.components.body;
                        var b = currentNeighbour.components.body;
                        var colliding = !(((a.pos.y + a.height) < b.pos.y) ||
                            (a.pos.y > (b.pos.y + b.height)) ||
                            ((a.pos.x + a.width) < b.pos.x) ||
                            (a.pos.x > (b.pos.x + b.width)));
                        if (colliding) {
                            // If both are collideable we can signal for collision but only
                            // if cooldown has happened.
                            if ((current.components.hasOwnProperty('collideable')) &&
                                (currentNeighbour.components.hasOwnProperty('collideable'))) {
                                if (current.components.collideable.timeUntilRefresh == 0) {
                                    that.broadcast(EventManager.Event.GAME_ENTITYCOLLISION, current, currentNeighbour);
                                }
                            }
                        }
                    });
                }
            }
        };
        return CollisionSystem;
    })(System);
    return CollisionSystem;
});
