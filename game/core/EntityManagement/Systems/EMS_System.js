var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../../EventManager/EventManager"], function (require, exports, EventManager) {
    var EMSSystem = (function (_super) {
        __extends(EMSSystem, _super);
        function EMSSystem() {
            _super.call(this);
            this.name = "";
        }
        EMSSystem.prototype.run = function (entities) {
            for (var key in entities) {
                if (!entities.hasOwnProperty(key))
                    continue;
                this.processItem(entities[key]);
            }
        };
        EMSSystem.prototype.processItem = function (item) { };
        return EMSSystem;
    })(EventManager.Subject);
    return EMSSystem;
});
