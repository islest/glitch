var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem"], function (require, exports, System) {
    var CooldownSystem = (function (_super) {
        __extends(CooldownSystem, _super);
        function CooldownSystem() {
            _super.call(this);
            this.name = "cooldown";
        }
        CooldownSystem.prototype.processItem = function (item) {
            // cooldown that occurs after a collision 
            if (item.components.hasOwnProperty('collideable')) {
                if (item.components.collideable.timeUntilRefresh > 0) {
                    item.components.collideable.timeUntilRefresh = item.components.collideable.timeUntilRefresh - 1;
                }
            }
            if (item.components.hasOwnProperty('shooting')) {
                if (item.components.shooting.cooldown > 0) {
                    item.components.shooting.cooldown = item.components.shooting.cooldown - 1;
                }
            }
            if (item.components.hasOwnProperty('health') && item.components.hasOwnProperty('decaying')) {
                if (item.components.health.health > 0) {
                    item.components.health.health = item.components.health.health - 1;
                }
            }
        };
        return CooldownSystem;
    })(System);
    return CooldownSystem;
});
