var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem"], function (require, exports, System) {
    var AnimationSystem = (function (_super) {
        __extends(AnimationSystem, _super);
        function AnimationSystem() {
            _super.call(this);
            this.name = "animation";
        }
        AnimationSystem.prototype.processItem = function (item) {
            if (!item.components.hasOwnProperty('graphics'))
                return;
            if (!item.components.hasOwnProperty('sprite'))
                return;
            if (!item.components.hasOwnProperty('body'))
                return;
            item.components.graphics.canvas = item.components.sprite.spriteSheet.getCurrentFrame();
            if (item.components.sprite.spriteSheet.autoplay) {
                item.components.sprite.spriteSheet.next();
            }
        };
        return AnimationSystem;
    })(System);
    return AnimationSystem;
});
