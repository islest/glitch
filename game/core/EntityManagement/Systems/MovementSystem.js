var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem"], function (require, exports, System) {
    var MovementSystem = (function (_super) {
        __extends(MovementSystem, _super);
        function MovementSystem() {
            _super.call(this);
            this.name = "movement";
        }
        MovementSystem.prototype.processItem = function (item) {
            if (!item.components.hasOwnProperty('body'))
                return;
            if (!item.components.hasOwnProperty('movement'))
                return;
            if (item.components.movement.moveType == "angled") {
                item.components.body.pos.x += item.components.movement.velocity * Math.cos(Math.PI / 180 * item.components.movement.angle);
                item.components.body.pos.y += item.components.movement.velocity * Math.sin(Math.PI / 180 * item.components.movement.angle);
            }
            else if (item.components.movement.moveType == "strafing") {
                item.components.body.pos.x += item.components.movement.velocity.x;
                item.components.body.pos.y += item.components.movement.velocity.y;
            }
        };
        return MovementSystem;
    })(System);
    return MovementSystem;
});
