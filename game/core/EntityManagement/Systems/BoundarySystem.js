var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem", "../../EventManager/EventManager", "../../SystemData/SystemData"], function (require, exports, System, EventManager, SystemData) {
    var BoundarySystem = (function (_super) {
        __extends(BoundarySystem, _super);
        function BoundarySystem() {
            _super.call(this);
            this.name = "boundary";
        }
        BoundarySystem.prototype.processItem = function (item) {
            if (!item.components.hasOwnProperty('body'))
                return;
            if (!item.components.hasOwnProperty('bounded')) {
                var entityOutOfBounds = false;
                entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.x > SystemData.SCREEN_WIDTH * 2);
                entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.x < -SystemData.SCREEN_WIDTH);
                entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.y > SystemData.SCREEN_HEIGHT * 2);
                entityOutOfBounds = entityOutOfBounds || (item.components.body.pos.y < -SystemData.SCREEN_HEIGHT);
                if (entityOutOfBounds) {
                    this.broadcast(EventManager.Event.GAME_ENTITYDEAD, item, null);
                }
            }
            else {
                var x = item.components.body.pos.x;
                var y = item.components.body.pos.y;
                var width = item.components.body.width;
                var height = item.components.body.height;
                if (x + width > SystemData.SCREEN_WIDTH) {
                    item.components.body.pos.x = SystemData.SCREEN_WIDTH - width;
                }
                if (x < 0) {
                    item.components.body.pos.x = 0;
                }
                if (y + height > SystemData.SCREEN_HEIGHT) {
                    item.components.body.pos.y = SystemData.SCREEN_HEIGHT - height;
                }
                if (y < 0) {
                    item.components.body.pos.y = 0;
                }
            }
        };
        return BoundarySystem;
    })(System);
    return BoundarySystem;
});
