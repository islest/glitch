var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./EMSSystem"], function (require, exports, System) {
    var CollisionCooldownSystem = (function (_super) {
        __extends(CollisionCooldownSystem, _super);
        function CollisionCooldownSystem() {
            _super.call(this);
            this.name = "collisioncooldown";
        }
        CollisionCooldownSystem.prototype.processItem = function (item) {
            if (!item.components.hasOwnProperty('collideable'))
                return;
            if (item.components.collideable.isCollided) {
                item.components.collideable.timeUntilRefresh -= 1;
            }
            if (item.components.collideable.timeUntilRefresh == 0) {
                item.components.collideable.isCollided = false;
            }
        };
        return CollisionCooldownSystem;
    })(System);
    return CollisionCooldownSystem;
});
