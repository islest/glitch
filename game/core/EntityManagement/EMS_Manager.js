var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../EventManager/EventManager", "./EMS_Assembler", "./Systems/RenderingSystem", "./Systems/MovementSystem", "./Systems/InputSystem", "./Systems/BoundarySystem", "./Systems/AnimationSystem"], function (require, exports, EventManager, EntityCreator, RenderingSystem, MovementSystem, InputSystem, BoundarySystem, AnimationSystem) {
    var EMSManager = (function (_super) {
        __extends(EMSManager, _super);
        function EMSManager() {
            _super.call(this);
            this.entities = {};
            this.systems = {};
            this.addSystem(new MovementSystem());
            this.addSystem(new BoundarySystem());
            this.addSystem(new InputSystem());
            this.addSystem(new RenderingSystem());
            this.addSystem(new AnimationSystem());
            this.addEntity(EntityCreator.createPlayer(100, 150));
            this.addEntity(EntityCreator.createEnemy(120, 160, 4, 90));
            this.addEntity(EntityCreator.createEnemy(100, 100, 4, 45));
            this.addEntity(EntityCreator.createEnemy(160, 200, -2, 270));
        }
        EMSManager.prototype.addEntity = function (entity) {
            this.entities[entity.id] = entity;
        };
        EMSManager.prototype.addSystem = function (system) {
            this.systems[system.name] = system;
            system.register(this);
        };
        EMSManager.prototype.notify = function (e, source) {
            if (e == EventManager.Event.GAME_BULLETFIRED) {
                var x = source.components.body.x + source.components.body.width / 2;
                var y = source.components.body.y + source.components.body.height / 2;
                var angle = 0;
                this.addEntity(EntityCreator.createSquare(x, y, 10, angle));
            }
            else if (e == EventManager.Event.GAME_ENTITYDEAD) {
                delete this.entities[source.id];
                console.log("Entity Deleted!");
            }
        };
        EMSManager.prototype.handleInput = function (input) {
            this.systems.input.inputHandler = input;
            this.systems.input.run(this.entities);
        };
        EMSManager.prototype.update = function () {
            this.systems.movement.run(this.entities);
            this.systems.boundary.run(this.entities);
        };
        EMSManager.prototype.render = function (canvas) {
            this.systems.animation.run(this.entities);
            this.systems.render.canvas = canvas;
            this.systems.render.run(this.entities);
        };
        return EMSManager;
    })(EventManager.Subject);
    return EMSManager;
});
