define(["require", "exports", "./EMSEntity", "./Components/Body", "./Components/AngledMovement", "./Components/StrafingMovement", "./Components/Graphics", "./Components/Controllable", "./Components/Health", "./Components/Shooter", "./Components/Bounded", "./Components/Sprite", "../Images/SpriteSheet", "./Components/Collideable", "./Components/Decaying", "../Game/AssetManager"], function (require, exports, EntityObject, BodyComponent, AngledMovementComponent, StrafingMovementComponent, GraphicComponent, ControllableComponent, HealthComponent, ShootingComponent, BoundedComponent, SpriteComponent, SpriteSheet, CollisionComponent, DecayingComponent, AssetManager) {
    ;
    function player(x, y, width, height) {
        // Instantiate object, load assets
        var player = new EntityObject("PLAYER");
        var spriteImageFile = AssetManager.getInstance().retrieveAsset('images/spaceship_spritesheet_32x32.png');
        var spriteSheet = new SpriteSheet(spriteImageFile, 32, 32, 3);
        // Load Components
        var body = new BodyComponent(x, y, width, height);
        var movement = new StrafingMovementComponent(0, 0, 0, 0);
        var graphic = new GraphicComponent(null);
        var controllable = new ControllableComponent();
        var sprite = new SpriteComponent(spriteSheet);
        player.addComponent(body);
        player.addComponent(movement);
        player.addComponent(graphic);
        player.addComponent(controllable);
        player.addComponent(sprite);
        player.addComponent(new ShootingComponent(15, 100));
        player.addComponent(new BoundedComponent());
        player.addComponent(new CollisionComponent(0));
        return player;
    }
    exports.player = player;
    function explosion(x, y, width, height) {
        var explosion = new EntityObject("EXPLOSION");
        var spriteImageFile = AssetManager.getInstance().retrieveAsset('images/explosion.png');
        var spriteSheet = new SpriteSheet(spriteImageFile, 32, 32, 7);
        spriteSheet.looping = true;
        spriteSheet.autoplay = true;
        explosion.addComponent(new BodyComponent(x, y, width, height));
        explosion.addComponent(new GraphicComponent(null));
        explosion.addComponent(new SpriteComponent(spriteSheet));
        explosion.addComponent(new HealthComponent(14));
        explosion.addComponent(new DecayingComponent());
        return explosion;
    }
    exports.explosion = explosion;
    function smallExplosion(x, y, width, height) {
        var explosion = new EntityObject("EXPLOSION");
        var spriteImageFile = AssetManager.getInstance().retrieveAsset('images/small_explosion_8x8.png');
        var spriteSheet = new SpriteSheet(spriteImageFile, 32, 32, 3);
        spriteSheet.looping = true;
        spriteSheet.autoplay = true;
        explosion.addComponent(new BodyComponent(x, y, width, height));
        explosion.addComponent(new GraphicComponent(null));
        explosion.addComponent(new SpriteComponent(spriteSheet));
        explosion.addComponent(new HealthComponent(3));
        explosion.addComponent(new DecayingComponent());
        return explosion;
    }
    exports.smallExplosion = smallExplosion;
    function bullet(x, y, width, height, velocity, angle) {
        var bullet = new EntityObject("BULLET");
        var bulletImageFile = AssetManager.getInstance().retrieveAsset('images/bullet_4x4.png');
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(bulletImageFile, 0, 0, canvas.width, canvas.height);
        var body = new BodyComponent(x, y, canvas.width, canvas.height);
        var movement = new AngledMovementComponent(velocity, 0, angle);
        var graphic = new GraphicComponent(canvas);
        bullet.addComponent(body);
        bullet.addComponent(movement);
        bullet.addComponent(graphic);
        bullet.addComponent(new CollisionComponent(0));
        return bullet;
    }
    exports.bullet = bullet;
    function turretEnemy(x, y, width, height, velocity, angle) {
        var enemy = new EntityObject("ENEMY");
        var turretImageFile = AssetManager.getInstance().retrieveAsset('images/turretship_32x32.png');
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(turretImageFile, 0, 0, canvas.width, canvas.height);
        var body = new BodyComponent(x, y, width, height);
        var movement = new AngledMovementComponent(velocity, 0, angle);
        var graphic = new GraphicComponent(canvas);
        enemy.addComponent(body);
        enemy.addComponent(movement);
        enemy.addComponent(graphic);
        enemy.addComponent(new BoundedComponent());
        enemy.addComponent(new HealthComponent(3));
        enemy.addComponent(new CollisionComponent(0));
        return enemy;
    }
    exports.turretEnemy = turretEnemy;
    function chaserEnemy(x, y, width, height, velocity, angle) {
        var enemy = new EntityObject("ENEMY");
        var turretImageFile = AssetManager.getInstance().retrieveAsset('images/flyingship_32x32.png');
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(turretImageFile, 0, 0, canvas.width, canvas.height);
        var body = new BodyComponent(x, y, width, height);
        var movement = new AngledMovementComponent(velocity, 0, angle);
        var graphic = new GraphicComponent(canvas);
        enemy.addComponent(body);
        enemy.addComponent(movement);
        enemy.addComponent(graphic);
        //enemy.addComponent(new BoundedComponent());
        return enemy;
    }
    exports.chaserEnemy = chaserEnemy;
});
