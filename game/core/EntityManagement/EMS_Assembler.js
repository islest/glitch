define(["require", "exports", "./EMS_Entity", "./Components/Body", "./Components/AngledMovement", "./Components/StrafingMovement", "./Components/Graphics", "./Components/Controllable", "./Components/Shooter", "./Components/Bounded", "./Components/Sprite", "../Images/SpriteSheet"], function (require, exports, EntityObject, BodyComponent, AngledMovementComponent, StrafingMovementComponent, GraphicComponent, ControllableComponent, ShootingComponent, BoundedComponent, SpriteComponent, SpriteSheet) {
    ;
    var EntityCreator = (function () {
        function EntityCreator() {
        }
        EntityCreator.createPlayer = function (x, y) {
            var player = new EntityObject("player");
            var canvas = document.createElement('canvas');
            canvas.width = 32;
            canvas.height = 32;
            var context = canvas.getContext('2d');
            var img = document.createElement('img');
            img.src = "../../../resources/images/spaceship_32x32.png";
            img.onload = function () {
                console.log("IMAGE IS LOADED");
                context.drawImage(img, 0, 0, 32, 32);
            };
            canvas.width = 32;
            canvas.height = 32;
            var w = canvas.width;
            var h = canvas.height;
            var body = new BodyComponent(x, y, canvas.width, canvas.height);
            var movement = new StrafingMovementComponent(0, 0, 0, 0);
            var graphic = new GraphicComponent(canvas);
            var controllable = new ControllableComponent();
            var s = new SpriteSheet("../../../resources/images/spaceship_spritesheet_32x32.png", 32, 32, 3);
            var sprite = new SpriteComponent(s);
            player.addComponent(body);
            player.addComponent(movement);
            player.addComponent(graphic);
            player.addComponent(controllable);
            player.addComponent(sprite);
            player.addComponent(new ShootingComponent(10, 100));
            player.addComponent(new BoundedComponent());
            return player;
        };
        EntityCreator.createSquare = function (x, y, velocity, angle) {
            var square = new EntityObject("bullet");
            var canvas = document.createElement('canvas');
            canvas.width = 8;
            canvas.height = 8;
            var ctx = canvas.getContext('2d');
            ctx.fillStyle = "#00FF00";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var body = new BodyComponent(x, y, canvas.width, canvas.height);
            var movement = new AngledMovementComponent(velocity, 0, angle);
            var graphic = new GraphicComponent(canvas);
            square.addComponent(body);
            square.addComponent(movement);
            square.addComponent(graphic);
            return square;
        };
        EntityCreator.createEnemy = function (x, y, velocity, angle) {
            var enemy = new EntityObject("enemy");
            var canvas = document.createElement('canvas');
            canvas.width = 32;
            canvas.height = 32;
            var ctx = canvas.getContext('2d');
            var img = document.createElement('img');
            img.src = "../../../resources/images/turretship_32x32.png";
            img.onload = function () {
                ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            };
            var body = new BodyComponent(x, y, canvas.width, canvas.height);
            var movement = new AngledMovementComponent(velocity, 0, angle);
            var graphic = new GraphicComponent(canvas);
            enemy.addComponent(body);
            enemy.addComponent(movement);
            enemy.addComponent(graphic);
            enemy.addComponent(new BoundedComponent());
            return enemy;
        };
        return EntityCreator;
    })();
    return EntityCreator;
});
