define(["require", "exports"], function (require, exports) {
    var EntityObject = (function () {
        function EntityObject(entityType) {
            var date = new Date();
            this.id = date.getMinutes().toString();
            this.id += date.getSeconds().toString();
            this.id += date.getMilliseconds().toString();
            this.id += (Math.random() * 100000000 | 0).toString();
            this.id += EntityObject.numEntities.toString();
            this.entityType = entityType;
            this.components = {};
            EntityObject.numEntities++;
        }
        EntityObject.prototype.addComponent = function (component) {
            this.components[component.name] = component;
        };
        EntityObject.prototype.removeComponent = function (componentName) {
            if (componentName in this.components) {
                this.components[componentName] = null;
            }
        };
        EntityObject.numEntities = 0;
        return EntityObject;
    })();
    return EntityObject;
});
