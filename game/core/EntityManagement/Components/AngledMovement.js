define(["require", "exports"], function (require, exports) {
    var AngledMovementComponent = (function () {
        function AngledMovementComponent(velocity, acceleration, angle) {
            this.name = "movement";
            this.moveType = "angled";
            this.velocity = velocity || 0;
            this.acceleration = acceleration || 0;
            this.angle = angle || 0;
        }
        return AngledMovementComponent;
    })();
    return AngledMovementComponent;
});
