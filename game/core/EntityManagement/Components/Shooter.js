define(["require", "exports"], function (require, exports) {
    var ShootingComponent = (function () {
        function ShootingComponent(fireRate, ammo) {
            this.name = "shooting";
            this.fireRate = fireRate;
            this.ammo = ammo;
            this.cooldown = 0;
        }
        return ShootingComponent;
    })();
    return ShootingComponent;
});
