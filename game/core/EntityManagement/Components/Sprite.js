define(["require", "exports"], function (require, exports) {
    var SpriteComponent = (function () {
        function SpriteComponent(sprite) {
            this.name = "sprite";
            this.spriteSheet = sprite;
        }
        return SpriteComponent;
    })();
    return SpriteComponent;
});
