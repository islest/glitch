define(["require", "exports"], function (require, exports) {
    var BoundedComponent = (function () {
        function BoundedComponent() {
            this.name = "bounded";
        }
        return BoundedComponent;
    })();
    return BoundedComponent;
});
