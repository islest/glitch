define(["require", "exports", "../../DataStructures/Point"], function (require, exports, Point) {
    var BodyComponent = (function () {
        function BodyComponent(x, y, width, height) {
            this.name = "body";
            this.pos = new Point(x || 0, y || 0);
            this.width = width || 0;
            this.height = height || 0;
        }
        return BodyComponent;
    })();
    return BodyComponent;
});
