define(["require", "exports"], function (require, exports) {
    var GraphicsComponent = (function () {
        function GraphicsComponent(canvas) {
            this.name = "graphics";
            this.canvas = canvas || undefined;
        }
        return GraphicsComponent;
    })();
    return GraphicsComponent;
});
