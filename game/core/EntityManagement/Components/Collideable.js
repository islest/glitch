define(["require", "exports"], function (require, exports) {
    var CollideableComponent = (function () {
        function CollideableComponent(cooldownTime) {
            this.name = "collideable";
            this.timeUntilRefresh = 0;
        }
        return CollideableComponent;
    })();
    return CollideableComponent;
});
