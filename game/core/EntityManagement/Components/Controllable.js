define(["require", "exports"], function (require, exports) {
    var ControllableComponent = (function () {
        function ControllableComponent() {
            this.name = "controllable";
        }
        return ControllableComponent;
    })();
    return ControllableComponent;
});
