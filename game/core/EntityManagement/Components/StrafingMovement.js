define(["require", "exports", "../../DataStructures/Point"], function (require, exports, Point) {
    var StrafingMovementComponent = (function () {
        function StrafingMovementComponent(velocityX, velocityY, accelerationX, accelerationY) {
            this.name = "movement";
            this.moveType = "strafing";
            this.velocity = new Point(velocityX || 0, velocityY || 0);
            this.acceleration = new Point(accelerationX || 0, accelerationY || 0);
        }
        return StrafingMovementComponent;
    })();
    return StrafingMovementComponent;
});
