define(["require", "exports"], function (require, exports) {
    var HealthComponent = (function () {
        function HealthComponent(health) {
            this.name = "health";
            this.health = health || 0;
        }
        return HealthComponent;
    })();
    return HealthComponent;
});
