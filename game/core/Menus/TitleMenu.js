var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./BaseMenu", "../EventManager/EventManager"], function (require, exports, Menu, EventManager) {
    var TitleMenu = (function (_super) {
        __extends(TitleMenu, _super);
        function TitleMenu() {
            _super.call(this);
            var play = new Menu.MenuItem("Play");
            var about = new Menu.MenuItem("About");
            var exit = new Menu.MenuItem("Exit");
            this.items = [play, about, exit];
        }
        TitleMenu.prototype.activate = function () {
            if (this.currentItem == 0) {
                console.log("BROADCASTING STARTGAME EVENT");
                this.broadcast(EventManager.Event.SYSTEM_STARTGAME, null, null);
            }
        };
        TitleMenu.prototype.render = function (canvas) {
            var context = canvas.getContext('2d');
            context.font = "20px Arial";
            for (var i = 0; i < this.items.length; i++) {
                context.fillStyle = "#555555";
                if (i == this.currentItem) {
                    context.fillStyle = "#0000FF";
                    context.fillRect(this.pos.x - 20, this.pos.y + i * 100 - 10, 10, 10);
                    context.fillStyle = "#FFFFFF";
                }
                context.fillText(this.items[i].description, this.pos.x, this.pos.y + i * 100);
            }
        };
        return TitleMenu;
    })(Menu.BaseMenu);
    return TitleMenu;
});
