var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", '../EventManager/EventManager', "../DataStructures/Point"], function (require, exports, EventManager, Point) {
    var MenuItem = (function () {
        function MenuItem(description) {
            this.description = description;
        }
        return MenuItem;
    })();
    exports.MenuItem = MenuItem;
    var BaseMenu = (function (_super) {
        __extends(BaseMenu, _super);
        function BaseMenu() {
            _super.call(this);
            this.items = [];
            this.currentItem = 0;
            this.pos = new Point(0, 0);
        }
        BaseMenu.prototype.setPosition = function (x, y) {
            this.pos = new Point(x || 0, y || 0);
        };
        BaseMenu.prototype.moveUp = function () {
            this.currentItem -= 1;
            if (this.currentItem < 0) {
                this.currentItem = 0;
            }
        };
        BaseMenu.prototype.moveDown = function () {
            this.currentItem += 1;
            if (this.items.length < 1) {
                this.currentItem = 0;
            }
            else if (this.currentItem > this.items.length - 1) {
                this.currentItem = this.items.length - 1;
            }
        };
        BaseMenu.prototype.moveRight = function () {
            this.currentItem -= 1;
            if (this.currentItem < 0) {
                this.currentItem = 0;
            }
        };
        BaseMenu.prototype.moveLeft = function () {
            this.currentItem += 1;
            if (this.items.length < 1) {
                this.currentItem = 0;
            }
            else if (this.currentItem > this.items.length - 1) {
                this.currentItem = this.items.length - 1;
            }
        };
        BaseMenu.prototype.activate = function () { };
        BaseMenu.prototype.handleInput = function (input) {
            if (input.has('s')) {
                this.moveDown();
                console.log('Moving selection upwards!');
            }
            else if (input.has('w')) {
                this.moveUp();
                console.log('Moving selection downwards!');
            }
            if (input.has('RTN')) {
                this.activate();
            }
            input.flush();
        };
        BaseMenu.prototype.update = function () { };
        BaseMenu.prototype.render = function (canvas) { };
        return BaseMenu;
    })(EventManager.Subject);
    exports.BaseMenu = BaseMenu;
});
