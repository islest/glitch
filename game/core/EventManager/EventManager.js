define(["require", "exports"], function (require, exports) {
    (function (Event) {
        // System events
        Event[Event["SYSTEM_STARTGAME"] = 0] = "SYSTEM_STARTGAME";
        Event[Event["SYSTEM_GAMEOVER"] = 1] = "SYSTEM_GAMEOVER";
        Event[Event["SYSTEM_VICTORY"] = 2] = "SYSTEM_VICTORY";
        Event[Event["SYSTEM_PAUSE"] = 3] = "SYSTEM_PAUSE";
        Event[Event["GAME_ENTITYDEAD"] = 4] = "GAME_ENTITYDEAD";
        Event[Event["GAME_BULLETFIRED"] = 5] = "GAME_BULLETFIRED";
        Event[Event["GAME_ENTITYCOLLISION"] = 6] = "GAME_ENTITYCOLLISION";
    })(exports.Event || (exports.Event = {}));
    var Event = exports.Event;
    var Subject = (function () {
        function Subject() {
            this.observers = [];
        }
        Subject.prototype.broadcast = function (e, src, target) {
            for (var i = 0; i < this.observers.length; ++i) {
                var current = this.observers[i];
                current.notify(e, src, target);
            }
        };
        Subject.prototype.register = function (o) {
            this.observers.push(o);
        };
        return Subject;
    })();
    exports.Subject = Subject;
});
