// Well, this class needs fixing.
define(["require", "exports"], function (require, exports) {
    var InputHandler = (function () {
        function InputHandler() {
            this.map = {};
        }
        InputHandler.prototype.translateKeyCode = function (key) {
            var code = key.keyCode;
            // If key is spacebar
            if (code == 32)
                return ' ';
            // If key is enter
            if (code == 13)
                return 'RTN';
            // If key is alphanumeric
            if (code >= 48 && code <= 90) {
                return String.fromCharCode(code).toLowerCase();
            }
            // Key is invalid
            return null;
        };
        InputHandler.prototype.has = function (character) {
            if (character in this.map) {
                if (this.map[character] == true) {
                    return true;
                }
            }
            return false;
        };
        InputHandler.prototype.print = function () {
            //console.log(this.map);
        };
        InputHandler.prototype.register = function () {
            var inputHandler = this;
            window.addEventListener('keydown', function (e) { inputHandler.add(e); }, false);
            window.addEventListener('keyup', function (e) { inputHandler.remove(e); }, false);
        };
        InputHandler.prototype.add = function (keyEvent) {
            var keyCode = this.translateKeyCode(keyEvent);
            if (keyCode !== null) {
                this.map[keyCode] = true;
            }
        };
        InputHandler.prototype.remove = function (keyEvent) {
            var keyCode = this.translateKeyCode(keyEvent);
            if (keyCode !== undefined) {
                if (this.map[keyCode] !== undefined) {
                    delete this.map[keyCode];
                }
            }
        };
        InputHandler.prototype.flush = function () {
            this.map = {};
        };
        return InputHandler;
    })();
    return InputHandler;
});
