define(["require", "exports"], function (require, exports) {
    var AssetType;
    (function (AssetType) {
        AssetType[AssetType["IMAGE"] = 0] = "IMAGE";
        AssetType[AssetType["SOUND"] = 1] = "SOUND";
    })(AssetType || (AssetType = {}));
    var AssetManager = (function () {
        function AssetManager() {
            if (AssetManager.instance) {
                throw new Error("Instantiation Failed of AssetManager Singleton");
            }
            this.downloadQueue = [];
            this.successCount = 0;
            this.errorCount = 0;
            this.cache = {};
            this.rootResourcePath = "";
            AssetManager.instance = this;
        }
        // Facilitates singleton instance of AssetManager
        AssetManager.getInstance = function () {
            return this.instance;
        };
        // Assets are stored by filename, but we set the root directory, where the files
        // are located, using this function
        AssetManager.prototype.setRootResourcePath = function (rootDirectory) {
            this.rootResourcePath = rootDirectory;
        };
        // Adds a file to the pathname, along with the type it was.
        AssetManager.prototype.addAsset = function (filename, format) {
            var asset = { path: filename, assetType: format };
            this.downloadQueue.push(asset);
        };
        // Downloads everything in the download queue
        AssetManager.prototype.downloadAll = function () {
            console.log(this.downloadQueue.length);
            if (this.downloadQueue.length == 0) {
                that.downloadCallback();
            }
            for (var i = 0; i < this.downloadQueue.length; ++i) {
                var asset = this.downloadQueue[i];
                var resource = undefined;
                if (asset.assetType == AssetManager.AssetType.IMAGE) {
                    resource = new Image();
                }
                else if (asset.assetType == AssetManager.AssetType.SOUND) {
                    resource = new Audio();
                }
                var that = this;
                resource.addEventListener("load", function () {
                    that.successCount += 1;
                    if (that.isFinished()) {
                        that.downloadCallback();
                    }
                });
                resource.addEventListener("error", function () {
                    that.errorCount += 1;
                    console.log("Error!");
                    if (that.isFinished()) {
                        that.downloadCallback();
                    }
                });
                resource.src = "";
                resource.src = this.rootResourcePath.concat(asset.path);
                this.cache[asset.path] = resource;
            }
            console.log(this.cache);
        };
        // Checks if all the images have finished downloading
        AssetManager.prototype.isFinished = function () {
            return (this.downloadQueue.length == 0 || this.downloadQueue.length == (this.successCount + this.errorCount));
        };
        // Retrieves an asset by path name
        AssetManager.prototype.retrieveAsset = function (path) {
            if (path in this.cache) {
                return this.cache[path];
            }
            else {
                return null;
            }
        };
        AssetManager.AssetType = AssetType;
        AssetManager.instance = new AssetManager();
        return AssetManager;
    })();
    return AssetManager;
});
