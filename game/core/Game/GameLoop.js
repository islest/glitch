define(["require", "exports"], function (require, exports) {
    var GameLoop = (function () {
        function GameLoop() {
            this.canvas = null;
            this.updateCallback = function () { };
            this.renderCallback = function () { };
            this.now = 0;
            this.dt = 0;
            this.last = Date.now();
            this.step = 1 / GameLoop.FPS;
            this.running = true;
        }
        GameLoop.prototype.setUpdate = function (callback) {
            this.updateCallback = callback;
        };
        GameLoop.prototype.setRender = function (callback) {
            this.renderCallback = callback;
        };
        GameLoop.prototype.start = function () {
            this.running = true;
            this.loop();
        };
        GameLoop.prototype.stop = function () {
            this.running = false;
        };
        GameLoop.prototype.isRunning = function () {
            return this.running;
        };
        GameLoop.prototype.loop = function () {
            this.now = Date.now();
            if (this.running) {
                this.dt = this.dt + Math.min(1, (this.now - this.last) / 1000);
                while (this.dt > this.step) {
                    this.dt -= this.step;
                    this.updateCallback();
                }
                this.renderCallback();
                this.last = this.now;
                window.requestAnimationFrame(this.loop.bind(this));
            }
        };
        GameLoop.FPS = 60;
        return GameLoop;
    })();
    return GameLoop;
});
