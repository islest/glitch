// THIS CLASS NEEDS RECODING!
define(["require", "exports"], function (require, exports) {
    var Sprite = (function () {
        function Sprite(image, frameWidth, frameHeight, numFrames) {
            this.looping = false;
            this.autoplay = false;
            this.frames = [];
            this.index = 0;
            this.source = image;
            for (var i = 0; i < numFrames; ++i) {
                var canvas = document.createElement('canvas');
                canvas.width = frameWidth;
                canvas.height = frameHeight;
                var ctx = canvas.getContext('2d');
                ctx.fillStyle = "#FFFFFF";
                ctx.fillRect(0, 0, frameHeight, frameWidth);
                this.frames.push(canvas);
            }
            for (var i = 0; i < numFrames; ++i) {
                var canvas = this.frames[i];
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, frameWidth, frameHeight);
                context.drawImage(this.source, i * frameWidth, 0, frameWidth, frameHeight, 0, 0, frameWidth, frameHeight);
            }
        }
        Sprite.prototype.next = function () {
            var increment = 0;
            if (this.index == this.frames.length - 1) {
                if (this.looping) {
                    this.index = 0;
                }
            }
            else {
                this.index++;
            }
        };
        Sprite.prototype.previous = function () {
            var increment = 0;
            if (this.index == 0) {
                if (this.looping) {
                    increment = this.frames.length - 1;
                }
            }
            else {
                increment = -1;
            }
            this.index += increment;
        };
        Sprite.prototype.reset = function () {
            this.index = 0;
        };
        Sprite.prototype.getIndex = function () {
            return this.index;
        };
        Sprite.prototype.setIndex = function (i) {
            this.index = i;
        };
        Sprite.prototype.getCurrentFrame = function () {
            return this.frames[this.index];
        };
        Sprite.prototype.getFrameLength = function () {
            return this.frames.length;
        };
        return Sprite;
    })();
    return Sprite;
});
