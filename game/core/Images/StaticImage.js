define(["require", "exports"], function (require, exports) {
    var StaticImage = (function () {
        function StaticImage(imageFile, x, y) {
            this.image = imageFile;
            this.x = x || 0;
            this.y = y || 0;
            this.speed = 0;
            this.scrolling = false;
            this.pattern = false;
        }
        StaticImage.prototype.setSpeed = function (speed) {
            this.speed = speed;
        };
        StaticImage.prototype.setScrolling = function (scrolling) {
            this.scrolling = scrolling;
        };
        StaticImage.prototype.setPattern = function (pattern) {
            this.pattern = pattern;
        };
        StaticImage.prototype.update = function () {
            if (this.scrolling) {
                this.x -= this.speed;
                if (this.x <= 0) {
                    this.x += this.image.width;
                }
            }
        };
        StaticImage.prototype.render = function (canvas) {
            var context = canvas.getContext('2d');
            if (this.pattern) {
                context.drawImage(this.image, this.x - this.image.width, this.y - this.image.height);
                context.drawImage(this.image, this.x - this.image.width, this.y);
                context.drawImage(this.image, this.x - this.image.width, this.y + this.image.height);
                context.drawImage(this.image, this.x, this.y - this.image.height);
                context.drawImage(this.image, this.x, this.y);
                context.drawImage(this.image, this.x, this.y + this.image.height);
                context.drawImage(this.image, this.x + this.image.width, this.y - this.image.height);
                context.drawImage(this.image, this.x + this.image.width, this.y);
                context.drawImage(this.image, this.x + this.image.width, this.y + this.image.height);
            }
            else {
                context.drawImage(this.image, this.x, this.y);
            }
        };
        return StaticImage;
    })();
    return StaticImage;
});
