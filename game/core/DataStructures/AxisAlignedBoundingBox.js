define(["require", "exports", "./Point", "./Box"], function (require, exports, Point, Box) {
    var AxisAlignedBoundingBox = (function () {
        function AxisAlignedBoundingBox(id, x, y, width, height) {
            this.position = new Point(x, y);
            this.dimensions = new Box(width, height);
            this.id = id;
        }
        return AxisAlignedBoundingBox;
    })();
    return AxisAlignedBoundingBox;
});
