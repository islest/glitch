define(["require", "exports"], function (require, exports) {
    //Comment
    var Box = (function () {
        function Box(width, height) {
            this.width = width || 0;
            this.height = height || 0;
        }
        return Box;
    })();
    return Box;
});
