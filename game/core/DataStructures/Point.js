define(["require", "exports"], function (require, exports) {
    var Point = (function () {
        function Point(x, y) {
            this.x = x || 0;
            this.y = y || 0;
        }
        return Point;
    })();
    return Point;
});
