define(["require", "exports", './Game/Game'], function (require, exports, Game) {
    console.log("Init");
    var canvas = document.getElementById('canvas');
    var game = new Game(canvas);
    game.init();
});
