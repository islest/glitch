define(["require", "exports"], function (require, exports) {
    var SystemData = (function () {
        function SystemData() {
        }
        SystemData.SCREEN_WIDTH = 640;
        SystemData.SCREEN_HEIGHT = 480;
        SystemData.CENTER_X = SystemData.SCREEN_WIDTH / 2;
        SystemData.CENTER_Y = SystemData.SCREEN_HEIGHT / 2;
        SystemData.RESOURCE_PATH = "/resources/";
        return SystemData;
    })();
    return SystemData;
});
