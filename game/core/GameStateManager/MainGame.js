var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./BaseState", "../Images/StaticImage", "../SystemData/SystemData", "../Game/AssetManager"], function (require, exports, BaseState, StaticImage, SystemData, AssetManager) {
    var MainGame = (function (_super) {
        __extends(MainGame, _super);
        function MainGame() {
            _super.call(this);
            var cloudImage = AssetManager.getInstance().retrieveAsset('images/weird.png');
            var starImage = AssetManager.getInstance().retrieveAsset('images/background.png');
            var bgZero = new StaticImage(cloudImage, 0, 0);
            bgZero.setScrolling(false);
            var bgOne = new StaticImage(starImage, SystemData.SCREEN_WIDTH, SystemData.SCREEN_HEIGHT / 3);
            bgOne.setScrolling(true);
            bgOne.setPattern(true);
            bgOne.setSpeed(1);
            var bgTwo = new StaticImage(starImage, SystemData.SCREEN_WIDTH, 0);
            bgTwo.setScrolling(true);
            bgTwo.setPattern(true);
            bgTwo.setSpeed(2);
            var bgThree = new StaticImage(starImage, SystemData.SCREEN_WIDTH, SystemData.SCREEN_HEIGHT / 4);
            bgThree.setScrolling(true);
            bgThree.setPattern(true);
            bgThree.setSpeed(3);
            this.updateables.push(bgZero);
            this.updateables.push(bgOne);
            this.updateables.push(bgTwo);
            this.updateables.push(bgThree);
            this.renderables.push(bgZero);
            this.renderables.push(bgOne);
            this.renderables.push(bgTwo);
            this.renderables.push(bgThree);
        }
        MainGame.prototype.init = function () {
            console.log("Initialising Main Game Screen!");
        };
        MainGame.prototype.notify = function (e, source, target) {
            this.broadcast(e, this, target);
        };
        return MainGame;
    })(BaseState);
    return MainGame;
});
