var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./BaseState", "../Menus/TitleMenu", "../Images/StaticImage", "../SystemData/SystemData", "../Game/AssetManager"], function (require, exports, BaseState, TitleMenu, StaticImage, SystemData, AssetManager) {
    var TitleScreen = (function (_super) {
        __extends(TitleScreen, _super);
        function TitleScreen() {
            _super.call(this);
            var menu = new TitleMenu();
            menu.register(this);
            menu.setPosition(SystemData.CENTER_X, SystemData.CENTER_Y);
            var logoImage = AssetManager.getInstance().retrieveAsset("images/logo.png");
            var bgImage = AssetManager.getInstance().retrieveAsset("images/background.png");
            var logo = new StaticImage(logoImage, 70, 30);
            var bg = new StaticImage(bgImage, 640, 0);
            bg.setScrolling(true);
            bg.setPattern(true);
            var bg2 = new StaticImage(bgImage, 640, 240);
            bg2.setScrolling(true);
            bg2.setPattern(true);
            bg2.setSpeed(2);
            this.updateables.push(bg);
            this.updateables.push(bg2);
            this.updateables.push(logo);
            this.updateables.push(menu);
            this.renderables.push(bg);
            this.renderables.push(bg2);
            this.renderables.push(logo);
            this.renderables.push(menu);
            this.controllables.push(menu);
        }
        TitleScreen.prototype.init = function () {
            console.log("Initialising Title Screen!");
        };
        TitleScreen.prototype.notify = function (e, source, target) {
            this.broadcast(e, source, target);
        };
        return TitleScreen;
    })(BaseState);
    return TitleScreen;
});
