var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../EventManager/EventManager", "../EntityManagement/EMSManager"], function (require, exports, EventManager, EMSManager) {
    var BaseState = (function (_super) {
        __extends(BaseState, _super);
        function BaseState() {
            _super.call(this);
            this.updateables = [];
            this.renderables = [];
            this.controllables = [];
            this.entityManager = new EMSManager();
        }
        BaseState.prototype.init = function () { };
        BaseState.prototype.handleInput = function (input) {
            this.controllables.forEach(function (item) {
                item.handleInput(input);
            });
            this.entityManager.handleInput(input);
        };
        BaseState.prototype.update = function () {
            this.updateables.forEach(function (item) {
                item.update();
            });
            this.entityManager.update();
        };
        BaseState.prototype.render = function (canvas) {
            var ctx = canvas.getContext('2d');
            ctx.fillStyle = "#000000";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            this.renderables.forEach(function (item) {
                item.render(canvas);
            });
            this.entityManager.render(canvas);
        };
        BaseState.prototype.notify = function (e, src, target) { };
        return BaseState;
    })(EventManager.Subject);
    return BaseState;
});
