var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./TitleScreen", "./MainGame", "../EventManager/EventManager"], function (require, exports, TitleScreen, MainGame, EventManager) {
    var GameStateManager = (function (_super) {
        __extends(GameStateManager, _super);
        function GameStateManager() {
            _super.call(this);
            this.gameStack = [];
        }
        GameStateManager.prototype.init = function () {
            this.push(new TitleScreen());
        };
        GameStateManager.prototype.push = function (state) {
            state.register(this);
            this.gameStack.push(state);
        };
        GameStateManager.prototype.pop = function (state) {
            this.gameStack.pop();
        };
        GameStateManager.prototype.handleInput = function (input) {
            input.print();
            this.gameStack[this.gameStack.length - 1].handleInput(input);
        };
        GameStateManager.prototype.update = function () {
            this.gameStack[this.gameStack.length - 1].update();
        };
        GameStateManager.prototype.render = function (canvas) {
            this.gameStack[this.gameStack.length - 1].render(canvas);
        };
        GameStateManager.prototype.notify = function (e, src, target) {
            if (e == EventManager.Event.SYSTEM_STARTGAME) {
                console.log("CHANGING TO MAINGAME");
                this.push(new MainGame());
            }
        };
        return GameStateManager;
    })(EventManager.Subject);
    return GameStateManager;
});
